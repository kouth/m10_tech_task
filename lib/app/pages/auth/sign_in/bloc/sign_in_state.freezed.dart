// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sign_in_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SignInState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(UserModel userModel) found,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function() validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignInErrorState value) error,
    required TResult Function(_$SignInFoundState value) found,
    required TResult Function(_$SignInLoadingState value) loading,
    required TResult Function(_$SignInNotFoundState value) notFound,
    required TResult Function(_$SignInValidationErrorState value) validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignInStateCopyWith<$Res> {
  factory $SignInStateCopyWith(
          SignInState value, $Res Function(SignInState) then) =
      _$SignInStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignInStateCopyWithImpl<$Res> implements $SignInStateCopyWith<$Res> {
  _$SignInStateCopyWithImpl(this._value, this._then);

  final SignInState _value;
  // ignore: unused_field
  final $Res Function(SignInState) _then;
}

/// @nodoc
abstract class _$$_$SignInErrorStateCopyWith<$Res> {
  factory _$$_$SignInErrorStateCopyWith(_$_$SignInErrorState value,
          $Res Function(_$_$SignInErrorState) then) =
      __$$_$SignInErrorStateCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$$_$SignInErrorStateCopyWithImpl<$Res>
    extends _$SignInStateCopyWithImpl<$Res>
    implements _$$_$SignInErrorStateCopyWith<$Res> {
  __$$_$SignInErrorStateCopyWithImpl(
      _$_$SignInErrorState _value, $Res Function(_$_$SignInErrorState) _then)
      : super(_value, (v) => _then(v as _$_$SignInErrorState));

  @override
  _$_$SignInErrorState get _value => super._value as _$_$SignInErrorState;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$SignInErrorState(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_$SignInErrorState implements _$SignInErrorState {
  const _$_$SignInErrorState({required this.description});

  @override
  final String description;

  @override
  String toString() {
    return 'SignInState.error(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignInErrorState &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$SignInErrorStateCopyWith<_$_$SignInErrorState> get copyWith =>
      __$$_$SignInErrorStateCopyWithImpl<_$_$SignInErrorState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(UserModel userModel) found,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function() validation,
  }) {
    return error(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
  }) {
    return error?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignInErrorState value) error,
    required TResult Function(_$SignInFoundState value) found,
    required TResult Function(_$SignInLoadingState value) loading,
    required TResult Function(_$SignInNotFoundState value) notFound,
    required TResult Function(_$SignInValidationErrorState value) validation,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _$SignInErrorState implements SignInState {
  const factory _$SignInErrorState({required final String description}) =
      _$_$SignInErrorState;

  String get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SignInErrorStateCopyWith<_$_$SignInErrorState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SignInFoundStateCopyWith<$Res> {
  factory _$$_$SignInFoundStateCopyWith(_$_$SignInFoundState value,
          $Res Function(_$_$SignInFoundState) then) =
      __$$_$SignInFoundStateCopyWithImpl<$Res>;
  $Res call({UserModel userModel});

  $UserModelCopyWith<$Res> get userModel;
}

/// @nodoc
class __$$_$SignInFoundStateCopyWithImpl<$Res>
    extends _$SignInStateCopyWithImpl<$Res>
    implements _$$_$SignInFoundStateCopyWith<$Res> {
  __$$_$SignInFoundStateCopyWithImpl(
      _$_$SignInFoundState _value, $Res Function(_$_$SignInFoundState) _then)
      : super(_value, (v) => _then(v as _$_$SignInFoundState));

  @override
  _$_$SignInFoundState get _value => super._value as _$_$SignInFoundState;

  @override
  $Res call({
    Object? userModel = freezed,
  }) {
    return _then(_$_$SignInFoundState(
      userModel: userModel == freezed
          ? _value.userModel
          : userModel // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ));
  }

  @override
  $UserModelCopyWith<$Res> get userModel {
    return $UserModelCopyWith<$Res>(_value.userModel, (value) {
      return _then(_value.copyWith(userModel: value));
    });
  }
}

/// @nodoc

class _$_$SignInFoundState implements _$SignInFoundState {
  const _$_$SignInFoundState({required this.userModel});

  @override
  final UserModel userModel;

  @override
  String toString() {
    return 'SignInState.found(userModel: $userModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignInFoundState &&
            const DeepCollectionEquality().equals(other.userModel, userModel));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userModel));

  @JsonKey(ignore: true)
  @override
  _$$_$SignInFoundStateCopyWith<_$_$SignInFoundState> get copyWith =>
      __$$_$SignInFoundStateCopyWithImpl<_$_$SignInFoundState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(UserModel userModel) found,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function() validation,
  }) {
    return found(userModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
  }) {
    return found?.call(userModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (found != null) {
      return found(userModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignInErrorState value) error,
    required TResult Function(_$SignInFoundState value) found,
    required TResult Function(_$SignInLoadingState value) loading,
    required TResult Function(_$SignInNotFoundState value) notFound,
    required TResult Function(_$SignInValidationErrorState value) validation,
  }) {
    return found(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
  }) {
    return found?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (found != null) {
      return found(this);
    }
    return orElse();
  }
}

abstract class _$SignInFoundState implements SignInState {
  const factory _$SignInFoundState({required final UserModel userModel}) =
      _$_$SignInFoundState;

  UserModel get userModel => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SignInFoundStateCopyWith<_$_$SignInFoundState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SignInLoadingStateCopyWith<$Res> {
  factory _$$_$SignInLoadingStateCopyWith(_$_$SignInLoadingState value,
          $Res Function(_$_$SignInLoadingState) then) =
      __$$_$SignInLoadingStateCopyWithImpl<$Res>;
  $Res call({String? description});
}

/// @nodoc
class __$$_$SignInLoadingStateCopyWithImpl<$Res>
    extends _$SignInStateCopyWithImpl<$Res>
    implements _$$_$SignInLoadingStateCopyWith<$Res> {
  __$$_$SignInLoadingStateCopyWithImpl(_$_$SignInLoadingState _value,
      $Res Function(_$_$SignInLoadingState) _then)
      : super(_value, (v) => _then(v as _$_$SignInLoadingState));

  @override
  _$_$SignInLoadingState get _value => super._value as _$_$SignInLoadingState;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$SignInLoadingState(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_$SignInLoadingState implements _$SignInLoadingState {
  const _$_$SignInLoadingState({this.description});

  @override
  final String? description;

  @override
  String toString() {
    return 'SignInState.loading(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignInLoadingState &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$SignInLoadingStateCopyWith<_$_$SignInLoadingState> get copyWith =>
      __$$_$SignInLoadingStateCopyWithImpl<_$_$SignInLoadingState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(UserModel userModel) found,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function() validation,
  }) {
    return loading(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
  }) {
    return loading?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignInErrorState value) error,
    required TResult Function(_$SignInFoundState value) found,
    required TResult Function(_$SignInLoadingState value) loading,
    required TResult Function(_$SignInNotFoundState value) notFound,
    required TResult Function(_$SignInValidationErrorState value) validation,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _$SignInLoadingState implements SignInState {
  const factory _$SignInLoadingState({final String? description}) =
      _$_$SignInLoadingState;

  String? get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SignInLoadingStateCopyWith<_$_$SignInLoadingState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SignInNotFoundStateCopyWith<$Res> {
  factory _$$_$SignInNotFoundStateCopyWith(_$_$SignInNotFoundState value,
          $Res Function(_$_$SignInNotFoundState) then) =
      __$$_$SignInNotFoundStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_$SignInNotFoundStateCopyWithImpl<$Res>
    extends _$SignInStateCopyWithImpl<$Res>
    implements _$$_$SignInNotFoundStateCopyWith<$Res> {
  __$$_$SignInNotFoundStateCopyWithImpl(_$_$SignInNotFoundState _value,
      $Res Function(_$_$SignInNotFoundState) _then)
      : super(_value, (v) => _then(v as _$_$SignInNotFoundState));

  @override
  _$_$SignInNotFoundState get _value => super._value as _$_$SignInNotFoundState;
}

/// @nodoc

class _$_$SignInNotFoundState implements _$SignInNotFoundState {
  const _$_$SignInNotFoundState();

  @override
  String toString() {
    return 'SignInState.notFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_$SignInNotFoundState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(UserModel userModel) found,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function() validation,
  }) {
    return notFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
  }) {
    return notFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignInErrorState value) error,
    required TResult Function(_$SignInFoundState value) found,
    required TResult Function(_$SignInLoadingState value) loading,
    required TResult Function(_$SignInNotFoundState value) notFound,
    required TResult Function(_$SignInValidationErrorState value) validation,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
  }) {
    return notFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class _$SignInNotFoundState implements SignInState {
  const factory _$SignInNotFoundState() = _$_$SignInNotFoundState;
}

/// @nodoc
abstract class _$$_$SignInValidationErrorStateCopyWith<$Res> {
  factory _$$_$SignInValidationErrorStateCopyWith(
          _$_$SignInValidationErrorState value,
          $Res Function(_$_$SignInValidationErrorState) then) =
      __$$_$SignInValidationErrorStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_$SignInValidationErrorStateCopyWithImpl<$Res>
    extends _$SignInStateCopyWithImpl<$Res>
    implements _$$_$SignInValidationErrorStateCopyWith<$Res> {
  __$$_$SignInValidationErrorStateCopyWithImpl(
      _$_$SignInValidationErrorState _value,
      $Res Function(_$_$SignInValidationErrorState) _then)
      : super(_value, (v) => _then(v as _$_$SignInValidationErrorState));

  @override
  _$_$SignInValidationErrorState get _value =>
      super._value as _$_$SignInValidationErrorState;
}

/// @nodoc

class _$_$SignInValidationErrorState implements _$SignInValidationErrorState {
  const _$_$SignInValidationErrorState();

  @override
  String toString() {
    return 'SignInState.validation()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignInValidationErrorState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(UserModel userModel) found,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function() validation,
  }) {
    return validation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
  }) {
    return validation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(UserModel userModel)? found,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (validation != null) {
      return validation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignInErrorState value) error,
    required TResult Function(_$SignInFoundState value) found,
    required TResult Function(_$SignInLoadingState value) loading,
    required TResult Function(_$SignInNotFoundState value) notFound,
    required TResult Function(_$SignInValidationErrorState value) validation,
  }) {
    return validation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
  }) {
    return validation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignInErrorState value)? error,
    TResult Function(_$SignInFoundState value)? found,
    TResult Function(_$SignInLoadingState value)? loading,
    TResult Function(_$SignInNotFoundState value)? notFound,
    TResult Function(_$SignInValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (validation != null) {
      return validation(this);
    }
    return orElse();
  }
}

abstract class _$SignInValidationErrorState implements SignInState {
  const factory _$SignInValidationErrorState() = _$_$SignInValidationErrorState;
}
