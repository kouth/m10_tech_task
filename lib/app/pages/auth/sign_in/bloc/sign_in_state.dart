import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../data/api/local/models/user.dart';

part 'sign_in_state.freezed.dart';

@freezed
class SignInState with _$SignInState {
  const factory SignInState.error({
    required String description,
  }) = _$SignInErrorState;

  const factory SignInState.found({
    required UserModel userModel,
  }) = _$SignInFoundState;

  const factory SignInState.loading({
    String? description,
  }) = _$SignInLoadingState;

  const factory SignInState.notFound() = _$SignInNotFoundState;

  const factory SignInState.validation() = _$SignInValidationErrorState;
}
