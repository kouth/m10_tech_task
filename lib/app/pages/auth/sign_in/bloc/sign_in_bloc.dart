import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../../../data/shared/logger/app_logger.dart';
import '../../../../../data/api/local/service/user_service.dart';
import 'sign_in_event.dart';
import 'sign_in_state.dart';

@Injectable()
class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc({
    required this.service,
  }) : super(
          const SignInState.loading(),
        ) {
    on<SignInLoginEvent>(
      (event, emit) async {
        try {
          emit(const SignInState.loading());

          if (event.login.replaceAll(' ', '').isEmpty ||
              event.password.replaceAll(' ', '').isEmpty) {
            emit(
              const SignInState.validation(),
            );
            return;
          }

          final _result = await service.signIn(
            login: event.login,
            password: event.password,
          );

          if (_result != null) {
            emit(
              SignInState.found(
                userModel: _result,
              ),
            );
          } else {
            emit(
              const SignInState.notFound(),
            );
          }
        } catch (error, stackTrace) {
          AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
          emit(
            SignInState.error(
              description: error.toString(),
            ),
          );
        }
      },
    );
  }

  final UserService service;
}
