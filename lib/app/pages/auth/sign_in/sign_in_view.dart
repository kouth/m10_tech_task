import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/config/app_scaffold.dart';
import '../../../utils/config/navigation/routes.dart';
import '../../../utils/config/service/l10n/l10n_service.dart';
import '../../../utils/config/style/app_text_styles.dart';
import '../../../utils/service/bloc_factory.dart';
import '../../../widgets/app_bar.dart';
import '../../../widgets/app_text_field.dart';
import '../../main/main_view.dart';
import '../sign_up/sign_up_view.dart';
import 'bloc/sign_in_bloc.dart';
import 'bloc/sign_in_event.dart';
import 'bloc/sign_in_state.dart';

class SignInView extends StatefulWidget {
  const SignInView({
    Key? key,
  }) : super(key: key);

  static const String routeName = RouteKeys.signIn;

  @override
  State<SignInView> createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  final _loginController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _passwordController.dispose();
    _loginController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      child: BlocProvider(
        create: (BuildContext context) {
          return context.read<BlocFactory>().create<SignInBloc>();
        },
        child: BlocConsumer<SignInBloc, SignInState>(
          listener: (context, state) {
            state.whenOrNull(
              found: (userModel) {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  MainView.routeName,
                  (Route<dynamic> route) => false,
                );
              },
              validation: () {
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(
                        AppLocale.of(context).fillAllFields,
                      ),
                    ),
                  );
              },
              notFound: () {
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(
                        AppLocale.of(context).notFound,
                      ),
                    ),
                  );
              },
            );
          },
          builder: (context, state) {
            return Scaffold(
              appBar: ApplicationAppBar(
                title: AppLocale.of(context).signIn,
                centerTitle: true,
              ),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text(
                        AppLocale.of(context).signInDescription,
                        style: AppTextStyle.h5Regular(context),
                      ),
                    ),
                    const Divider(
                      height: 1,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AppTextFieldWidget(
                        controller: _loginController,
                        hintText: AppLocale.of(context).enterLogin,
                        labelText: AppLocale.of(context).login,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AppTextFieldWidget(
                        controller: _passwordController,
                        obscureText: true,
                        hintText: AppLocale.of(context).enterPassword,
                        labelText: AppLocale.of(context).password,
                      ),
                    ),
                    const SizedBox(height: 16),
                    const Divider(
                      height: 1,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: CupertinoButton(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.zero,
                        child: Text(
                          AppLocale.of(context).signUp,
                        ),
                        onPressed: () {
                          Navigator.pushNamed(
                            context,
                            SignUpView.routeName,
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: SafeArea(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(
                      height: 16,
                    ),
                    CupertinoButton(
                      padding: EdgeInsets.zero,
                      child: Text(
                        AppLocale.of(context).signIn,
                      ),
                      onPressed: () async {
                        context.read<SignInBloc>().add(
                              SignInEvent.login(
                                login: _loginController.text,
                                password: _passwordController.text,
                              ),
                            );
                      },
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
