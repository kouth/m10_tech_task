// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sign_up_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SignUpState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel userModel) created,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() userExist,
    required TResult Function() validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignUpCreated value) created,
    required TResult Function(_$SignUpError value) error,
    required TResult Function(_$SignUpLoading value) loading,
    required TResult Function(_$SignUpUserExist value) userExist,
    required TResult Function(_$SignUpValidationErrorState value) validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpStateCopyWith<$Res> {
  factory $SignUpStateCopyWith(
          SignUpState value, $Res Function(SignUpState) then) =
      _$SignUpStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpStateCopyWithImpl<$Res> implements $SignUpStateCopyWith<$Res> {
  _$SignUpStateCopyWithImpl(this._value, this._then);

  final SignUpState _value;
  // ignore: unused_field
  final $Res Function(SignUpState) _then;
}

/// @nodoc
abstract class _$$_$SignUpCreatedCopyWith<$Res> {
  factory _$$_$SignUpCreatedCopyWith(
          _$_$SignUpCreated value, $Res Function(_$_$SignUpCreated) then) =
      __$$_$SignUpCreatedCopyWithImpl<$Res>;
  $Res call({UserModel userModel});

  $UserModelCopyWith<$Res> get userModel;
}

/// @nodoc
class __$$_$SignUpCreatedCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements _$$_$SignUpCreatedCopyWith<$Res> {
  __$$_$SignUpCreatedCopyWithImpl(
      _$_$SignUpCreated _value, $Res Function(_$_$SignUpCreated) _then)
      : super(_value, (v) => _then(v as _$_$SignUpCreated));

  @override
  _$_$SignUpCreated get _value => super._value as _$_$SignUpCreated;

  @override
  $Res call({
    Object? userModel = freezed,
  }) {
    return _then(_$_$SignUpCreated(
      userModel: userModel == freezed
          ? _value.userModel
          : userModel // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ));
  }

  @override
  $UserModelCopyWith<$Res> get userModel {
    return $UserModelCopyWith<$Res>(_value.userModel, (value) {
      return _then(_value.copyWith(userModel: value));
    });
  }
}

/// @nodoc

class _$_$SignUpCreated implements _$SignUpCreated {
  const _$_$SignUpCreated({required this.userModel});

  @override
  final UserModel userModel;

  @override
  String toString() {
    return 'SignUpState.created(userModel: $userModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignUpCreated &&
            const DeepCollectionEquality().equals(other.userModel, userModel));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userModel));

  @JsonKey(ignore: true)
  @override
  _$$_$SignUpCreatedCopyWith<_$_$SignUpCreated> get copyWith =>
      __$$_$SignUpCreatedCopyWithImpl<_$_$SignUpCreated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel userModel) created,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() userExist,
    required TResult Function() validation,
  }) {
    return created(userModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
  }) {
    return created?.call(userModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (created != null) {
      return created(userModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignUpCreated value) created,
    required TResult Function(_$SignUpError value) error,
    required TResult Function(_$SignUpLoading value) loading,
    required TResult Function(_$SignUpUserExist value) userExist,
    required TResult Function(_$SignUpValidationErrorState value) validation,
  }) {
    return created(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
  }) {
    return created?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (created != null) {
      return created(this);
    }
    return orElse();
  }
}

abstract class _$SignUpCreated implements SignUpState {
  const factory _$SignUpCreated({required final UserModel userModel}) =
      _$_$SignUpCreated;

  UserModel get userModel => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SignUpCreatedCopyWith<_$_$SignUpCreated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SignUpErrorCopyWith<$Res> {
  factory _$$_$SignUpErrorCopyWith(
          _$_$SignUpError value, $Res Function(_$_$SignUpError) then) =
      __$$_$SignUpErrorCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$$_$SignUpErrorCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements _$$_$SignUpErrorCopyWith<$Res> {
  __$$_$SignUpErrorCopyWithImpl(
      _$_$SignUpError _value, $Res Function(_$_$SignUpError) _then)
      : super(_value, (v) => _then(v as _$_$SignUpError));

  @override
  _$_$SignUpError get _value => super._value as _$_$SignUpError;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$SignUpError(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_$SignUpError implements _$SignUpError {
  const _$_$SignUpError({required this.description});

  @override
  final String description;

  @override
  String toString() {
    return 'SignUpState.error(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignUpError &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$SignUpErrorCopyWith<_$_$SignUpError> get copyWith =>
      __$$_$SignUpErrorCopyWithImpl<_$_$SignUpError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel userModel) created,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() userExist,
    required TResult Function() validation,
  }) {
    return error(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
  }) {
    return error?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignUpCreated value) created,
    required TResult Function(_$SignUpError value) error,
    required TResult Function(_$SignUpLoading value) loading,
    required TResult Function(_$SignUpUserExist value) userExist,
    required TResult Function(_$SignUpValidationErrorState value) validation,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _$SignUpError implements SignUpState {
  const factory _$SignUpError({required final String description}) =
      _$_$SignUpError;

  String get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SignUpErrorCopyWith<_$_$SignUpError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SignUpLoadingCopyWith<$Res> {
  factory _$$_$SignUpLoadingCopyWith(
          _$_$SignUpLoading value, $Res Function(_$_$SignUpLoading) then) =
      __$$_$SignUpLoadingCopyWithImpl<$Res>;
  $Res call({String? description});
}

/// @nodoc
class __$$_$SignUpLoadingCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements _$$_$SignUpLoadingCopyWith<$Res> {
  __$$_$SignUpLoadingCopyWithImpl(
      _$_$SignUpLoading _value, $Res Function(_$_$SignUpLoading) _then)
      : super(_value, (v) => _then(v as _$_$SignUpLoading));

  @override
  _$_$SignUpLoading get _value => super._value as _$_$SignUpLoading;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$SignUpLoading(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_$SignUpLoading implements _$SignUpLoading {
  const _$_$SignUpLoading({this.description});

  @override
  final String? description;

  @override
  String toString() {
    return 'SignUpState.loading(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignUpLoading &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$SignUpLoadingCopyWith<_$_$SignUpLoading> get copyWith =>
      __$$_$SignUpLoadingCopyWithImpl<_$_$SignUpLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel userModel) created,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() userExist,
    required TResult Function() validation,
  }) {
    return loading(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
  }) {
    return loading?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignUpCreated value) created,
    required TResult Function(_$SignUpError value) error,
    required TResult Function(_$SignUpLoading value) loading,
    required TResult Function(_$SignUpUserExist value) userExist,
    required TResult Function(_$SignUpValidationErrorState value) validation,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _$SignUpLoading implements SignUpState {
  const factory _$SignUpLoading({final String? description}) =
      _$_$SignUpLoading;

  String? get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SignUpLoadingCopyWith<_$_$SignUpLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SignUpUserExistCopyWith<$Res> {
  factory _$$_$SignUpUserExistCopyWith(
          _$_$SignUpUserExist value, $Res Function(_$_$SignUpUserExist) then) =
      __$$_$SignUpUserExistCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_$SignUpUserExistCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements _$$_$SignUpUserExistCopyWith<$Res> {
  __$$_$SignUpUserExistCopyWithImpl(
      _$_$SignUpUserExist _value, $Res Function(_$_$SignUpUserExist) _then)
      : super(_value, (v) => _then(v as _$_$SignUpUserExist));

  @override
  _$_$SignUpUserExist get _value => super._value as _$_$SignUpUserExist;
}

/// @nodoc

class _$_$SignUpUserExist implements _$SignUpUserExist {
  const _$_$SignUpUserExist();

  @override
  String toString() {
    return 'SignUpState.userExist()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_$SignUpUserExist);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel userModel) created,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() userExist,
    required TResult Function() validation,
  }) {
    return userExist();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
  }) {
    return userExist?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (userExist != null) {
      return userExist();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignUpCreated value) created,
    required TResult Function(_$SignUpError value) error,
    required TResult Function(_$SignUpLoading value) loading,
    required TResult Function(_$SignUpUserExist value) userExist,
    required TResult Function(_$SignUpValidationErrorState value) validation,
  }) {
    return userExist(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
  }) {
    return userExist?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (userExist != null) {
      return userExist(this);
    }
    return orElse();
  }
}

abstract class _$SignUpUserExist implements SignUpState {
  const factory _$SignUpUserExist() = _$_$SignUpUserExist;
}

/// @nodoc
abstract class _$$_$SignUpValidationErrorStateCopyWith<$Res> {
  factory _$$_$SignUpValidationErrorStateCopyWith(
          _$_$SignUpValidationErrorState value,
          $Res Function(_$_$SignUpValidationErrorState) then) =
      __$$_$SignUpValidationErrorStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_$SignUpValidationErrorStateCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements _$$_$SignUpValidationErrorStateCopyWith<$Res> {
  __$$_$SignUpValidationErrorStateCopyWithImpl(
      _$_$SignUpValidationErrorState _value,
      $Res Function(_$_$SignUpValidationErrorState) _then)
      : super(_value, (v) => _then(v as _$_$SignUpValidationErrorState));

  @override
  _$_$SignUpValidationErrorState get _value =>
      super._value as _$_$SignUpValidationErrorState;
}

/// @nodoc

class _$_$SignUpValidationErrorState implements _$SignUpValidationErrorState {
  const _$_$SignUpValidationErrorState();

  @override
  String toString() {
    return 'SignUpState.validation()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SignUpValidationErrorState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel userModel) created,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() userExist,
    required TResult Function() validation,
  }) {
    return validation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
  }) {
    return validation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel userModel)? created,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? userExist,
    TResult Function()? validation,
    required TResult orElse(),
  }) {
    if (validation != null) {
      return validation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SignUpCreated value) created,
    required TResult Function(_$SignUpError value) error,
    required TResult Function(_$SignUpLoading value) loading,
    required TResult Function(_$SignUpUserExist value) userExist,
    required TResult Function(_$SignUpValidationErrorState value) validation,
  }) {
    return validation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
  }) {
    return validation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SignUpCreated value)? created,
    TResult Function(_$SignUpError value)? error,
    TResult Function(_$SignUpLoading value)? loading,
    TResult Function(_$SignUpUserExist value)? userExist,
    TResult Function(_$SignUpValidationErrorState value)? validation,
    required TResult orElse(),
  }) {
    if (validation != null) {
      return validation(this);
    }
    return orElse();
  }
}

abstract class _$SignUpValidationErrorState implements SignUpState {
  const factory _$SignUpValidationErrorState() = _$_$SignUpValidationErrorState;
}
