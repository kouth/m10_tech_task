import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../../../data/shared/logger/app_logger.dart';
import '../../../../../data/api/local/service/user_service.dart';
import 'sign_up_event.dart';
import 'sign_up_state.dart';

@Injectable()
class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignUpBloc({
    required this.service,
  }) : super(
          const SignUpState.loading(),
        ) {
    on<SignUpRegisterEvent>(
      (event, emit) async {
        try {
          emit(const SignUpState.loading());

          if (event.login.replaceAll(' ', '').isEmpty ||
              event.password.replaceAll(' ', '').isEmpty ||
              event.name.replaceAll(' ', '').isEmpty) {
            emit(
              const SignUpState.validation(),
            );
            return;
          }

          final _result = await service.signUp(
            login: event.login,
            password: event.password,
            date: event.date,
            name: event.name,
          );

          if (_result != null) {
            emit(
              SignUpState.created(
                userModel: _result,
              ),
            );
          } else {
            emit(
              const SignUpState.userExist(),
            );
          }
        } catch (error, stackTrace) {
          AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
          emit(
            SignUpState.error(
              description: error.toString(),
            ),
          );
        }
      },
    );
  }

  final UserService service;
}
