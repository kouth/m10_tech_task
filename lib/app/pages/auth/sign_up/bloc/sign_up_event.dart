import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_up_event.freezed.dart';

@freezed
class SignUpEvent with _$SignUpEvent {
  const factory SignUpEvent.register({
    required String name,
    required String login,
    required String password,
    required DateTime date,
  }) = SignUpRegisterEvent;
}
