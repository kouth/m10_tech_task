import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../data/api/local/models/user.dart';

part 'sign_up_state.freezed.dart';

@freezed
class SignUpState with _$SignUpState {
  const factory SignUpState.created({
    required UserModel userModel,
  }) = _$SignUpCreated;

  const factory SignUpState.error({
    required String description,
  }) = _$SignUpError;

  const factory SignUpState.loading({
    String? description,
  }) = _$SignUpLoading;

  const factory SignUpState.userExist() = _$SignUpUserExist;

  const factory SignUpState.validation() = _$SignUpValidationErrorState;
}
