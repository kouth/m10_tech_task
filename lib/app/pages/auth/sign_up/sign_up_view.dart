import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/config/app_scaffold.dart';
import '../../../utils/config/navigation/routes.dart';
import '../../../utils/config/service/l10n/l10n_service.dart';
import '../../../utils/config/style/app_text_styles.dart';
import '../../../utils/service/bloc_factory.dart';
import '../../../widgets/app_bar.dart';
import '../../../widgets/app_text_field.dart';
import '../../main/main_view.dart';
import 'bloc/sign_up_bloc.dart';
import 'bloc/sign_up_event.dart';
import 'bloc/sign_up_state.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({
    Key? key,
  }) : super(key: key);

  static const String routeName = RouteKeys.signUp;

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  final _loginController = TextEditingController();
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _passwordController.dispose();
    _loginController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      child: BlocProvider(
        create: (BuildContext context) {
          return context.read<BlocFactory>().create<SignUpBloc>();
        },
        child: BlocConsumer<SignUpBloc, SignUpState>(
          listener: (context, state) {
            state.whenOrNull(
              created: (userModel) {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  MainView.routeName,
                  (Route<dynamic> route) => false,
                );
              },
              validation: () {
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(
                        AppLocale.of(context).fillAllFields,
                      ),
                    ),
                  );
              },
              userExist: () {
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(
                        AppLocale.of(context).userExists,
                      ),
                    ),
                  );
              },
            );
          },
          builder: (context, state) {
            return Scaffold(
              appBar: ApplicationAppBar(
                title: AppLocale.of(context).signUp,
                centerTitle: true,
              ),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text(
                        AppLocale.of(context).signUpDescription,
                        style: AppTextStyle.h5Regular(context),
                      ),
                    ),
                    const Divider(
                      height: 1,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AppTextFieldWidget(
                        controller: _nameController,
                        hintText: AppLocale.of(context).enterName,
                        labelText: AppLocale.of(context).name,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AppTextFieldWidget(
                        controller: _loginController,
                        hintText: AppLocale.of(context).enterLogin,
                        labelText: AppLocale.of(context).login,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AppTextFieldWidget(
                        controller: _passwordController,
                        obscureText: true,
                        hintText: AppLocale.of(context).enterPassword,
                        labelText: AppLocale.of(context).password,
                      ),
                    ),
                    const SizedBox(height: 16),
                    const Divider(
                      height: 1,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: CupertinoButton(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.zero,
                        child: Text(
                          AppLocale.of(context).signIn,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: SafeArea(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(
                      height: 16,
                    ),
                    CupertinoButton(
                      padding: EdgeInsets.zero,
                      child: Text(
                        AppLocale.of(context).signUp,
                      ),
                      onPressed: () {
                        context.read<SignUpBloc>().add(
                              SignUpEvent.register(
                                login: _loginController.text,
                                password: _passwordController.text,
                                date: DateTime.now(),
                                name: _nameController.text,
                              ),
                            );
                      },
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
