import 'package:flutter/material.dart';

import '../../../utils/config/app_scaffold.dart';
import '../../../utils/config/navigation/routes.dart';
import '../../../utils/config/service/l10n/l10n_service.dart';
import '../../../utils/config/style/app_text_styles.dart';

class NoRouteView extends StatelessWidget {
  const NoRouteView({
    Key? key,
    required this.text,
  }) : super(key: key);

  static const String routeName = RouteKeys.routeNotFound;

  final String text;

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      child: Center(
        child: Text(
          '${AppLocale.of(context).routeNotFound} $text',
          style: AppTextStyle.h5Bold(context),
        ),
      ),
    );
  }
}
