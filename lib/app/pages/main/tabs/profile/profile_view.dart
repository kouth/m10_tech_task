import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../data/shared/l10n/language/language_controller.dart';
import '../../../../utils/config/app_scaffold.dart';
import '../../../../utils/config/navigation/routes.dart';
import '../../../../utils/config/service/l10n/l10n_service.dart';
import '../../../../utils/config/style/app_colors.dart';
import '../../../../utils/config/style/app_text_styles.dart';
import '../../../../utils/service/bloc_factory.dart';
import '../../../../widgets/app_bar.dart';
import '../../../../widgets/app_text_tile.dart';
import '../../../auth/sign_in/sign_in_view.dart';
import 'bloc/profile_bloc.dart';
import 'bloc/profile_event.dart';
import 'bloc/profile_state.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({
    Key? key,
  }) : super(key: key);

  static const String routeName = RouteKeys.profile;

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return AppScaffold(
      child: Scaffold(
        appBar: ApplicationAppBar(
          title: AppLocale.of(context).profile,
          centerTitle: true,
        ),
        body: BlocProvider(
          create: (BuildContext context) {
            return context.read<BlocFactory>().create<ProfileBloc>()
              ..add(
                const ProfileEvent.fetchUser(),
              );
          },
          child: BlocConsumer<ProfileBloc, ProfileState>(
            listener: (context, state) {
              state.whenOrNull(
                notFound: () {
                  ScaffoldMessenger.of(context)
                    ..removeCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                        content: Text(
                          AppLocale.of(context).notFound,
                        ),
                      ),
                    );
                },
              );
            },
            builder: (context, state) {
              return state.when(
                error: (description) {
                  return Center(
                    child: TextButton(
                      onPressed: () {
                        context.read<ProfileBloc>().add(
                              const ProfileEvent.fetchUser(),
                            );
                      },
                      child: Text(
                        AppLocale.of(context).errorWithMessage(description),
                      ),
                    ),
                  );
                },
                success: (_userModel) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16),
                        child: Text(
                          AppLocale.of(context).userProfile(
                            _userModel.login,
                          ),
                          style: AppTextStyle.h5Regular(context),
                        ),
                      ),
                      const Divider(
                        height: 1,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppTextTileWidget(
                              title: AppLocale.of(context).login,
                              value: _userModel.login,
                            ),
                            const SizedBox(height: 16),
                            AppTextTileWidget(
                              title: AppLocale.of(context).name,
                              value: _userModel.name,
                            ),
                            const SizedBox(height: 16),
                            AppTextTileWidget(
                              title: AppLocale.of(context).password,
                              value: _userModel.password,
                            ),
                            const SizedBox(height: 16),
                            AppTextTileWidget(
                              title: AppLocale.of(context).date,
                              value: _userModel.date.toIso8601String(),
                            ),
                            const SizedBox(height: 16),
                          ],
                        ),
                      ),
                      const Divider(
                        height: 1,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: CupertinoButton(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.zero,
                          child: Text(
                            AppLocale.of(context).language,
                          ),
                          onPressed: () {
                            context
                                .read<LocaleSettingsController>()
                                .updateLocale();
                          },
                        ),
                      ),
                      const Divider(
                        height: 1,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: CupertinoButton(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.zero,
                          child: Text(
                            AppLocale.of(context).clearDb,
                            style: AppTextStyle.h4Regular(context)?.copyWith(
                              color: AppColors.red,
                            ),
                          ),
                          onPressed: () {
                            context.read<ProfileBloc>().add(
                                  const ProfileEvent.clearDatabase(),
                                );
                            Navigator.pushNamedAndRemoveUntil(
                              context,
                              SignInView.routeName,
                              (Route<dynamic> route) => false,
                            );
                          },
                        ),
                      ),
                      const Divider(
                        height: 1,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: CupertinoButton(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.zero,
                          child: Text(
                            AppLocale.of(context).exit,
                            style: AppTextStyle.h4Regular(context)?.copyWith(
                              color: AppColors.red,
                            ),
                          ),
                          onPressed: () async {
                            context.read<ProfileBloc>().add(
                                  const ProfileEvent.logOut(),
                                );
                            await Navigator.pushNamedAndRemoveUntil(
                              context,
                              SignInView.routeName,
                              (Route<dynamic> route) => false,
                            );
                          },
                        ),
                      ),
                    ],
                  );
                },
                loading: (description) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppLocale.of(context).loading,
                      ),
                      const Padding(
                        padding: EdgeInsets.all(16),
                        child: LinearProgressIndicator(),
                      ),
                    ],
                  );
                },
                notFound: () {
                  return Center(
                    child: TextButton(
                      onPressed: () {
                        context.read<ProfileBloc>().add(
                              const ProfileEvent.logOut(),
                            );
                      },
                      child: Text(
                        AppLocale.of(context).notFound,
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
