import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_event.freezed.dart';

@freezed
class ProfileEvent with _$ProfileEvent {
  const factory ProfileEvent.clearDatabase() = ProfileClearDatabaseEvent;

  const factory ProfileEvent.fetchUser() = ProfileFetchUserEvent;

  const factory ProfileEvent.logOut() = ProfileLogOutEvent;
}
