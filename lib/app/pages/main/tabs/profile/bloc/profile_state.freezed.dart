// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'profile_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function(UserModel userModel) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$ProfileError value) error,
    required TResult Function(_$ProfileLoading value) loading,
    required TResult Function(_$ProfileStateNotFoundState value) notFound,
    required TResult Function(_$ProfileSuccess value) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileStateCopyWith<$Res> {
  factory $ProfileStateCopyWith(
          ProfileState value, $Res Function(ProfileState) then) =
      _$ProfileStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProfileStateCopyWithImpl<$Res> implements $ProfileStateCopyWith<$Res> {
  _$ProfileStateCopyWithImpl(this._value, this._then);

  final ProfileState _value;
  // ignore: unused_field
  final $Res Function(ProfileState) _then;
}

/// @nodoc
abstract class _$$_$ProfileErrorCopyWith<$Res> {
  factory _$$_$ProfileErrorCopyWith(
          _$_$ProfileError value, $Res Function(_$_$ProfileError) then) =
      __$$_$ProfileErrorCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$$_$ProfileErrorCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$$_$ProfileErrorCopyWith<$Res> {
  __$$_$ProfileErrorCopyWithImpl(
      _$_$ProfileError _value, $Res Function(_$_$ProfileError) _then)
      : super(_value, (v) => _then(v as _$_$ProfileError));

  @override
  _$_$ProfileError get _value => super._value as _$_$ProfileError;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$ProfileError(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_$ProfileError implements _$ProfileError {
  const _$_$ProfileError({required this.description});

  @override
  final String description;

  @override
  String toString() {
    return 'ProfileState.error(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$ProfileError &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$ProfileErrorCopyWith<_$_$ProfileError> get copyWith =>
      __$$_$ProfileErrorCopyWithImpl<_$_$ProfileError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function(UserModel userModel) success,
  }) {
    return error(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
  }) {
    return error?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$ProfileError value) error,
    required TResult Function(_$ProfileLoading value) loading,
    required TResult Function(_$ProfileStateNotFoundState value) notFound,
    required TResult Function(_$ProfileSuccess value) success,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _$ProfileError implements ProfileState {
  const factory _$ProfileError({required final String description}) =
      _$_$ProfileError;

  String get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$ProfileErrorCopyWith<_$_$ProfileError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$ProfileLoadingCopyWith<$Res> {
  factory _$$_$ProfileLoadingCopyWith(
          _$_$ProfileLoading value, $Res Function(_$_$ProfileLoading) then) =
      __$$_$ProfileLoadingCopyWithImpl<$Res>;
  $Res call({String? description});
}

/// @nodoc
class __$$_$ProfileLoadingCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$$_$ProfileLoadingCopyWith<$Res> {
  __$$_$ProfileLoadingCopyWithImpl(
      _$_$ProfileLoading _value, $Res Function(_$_$ProfileLoading) _then)
      : super(_value, (v) => _then(v as _$_$ProfileLoading));

  @override
  _$_$ProfileLoading get _value => super._value as _$_$ProfileLoading;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$ProfileLoading(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_$ProfileLoading implements _$ProfileLoading {
  const _$_$ProfileLoading({this.description});

  @override
  final String? description;

  @override
  String toString() {
    return 'ProfileState.loading(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$ProfileLoading &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$ProfileLoadingCopyWith<_$_$ProfileLoading> get copyWith =>
      __$$_$ProfileLoadingCopyWithImpl<_$_$ProfileLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function(UserModel userModel) success,
  }) {
    return loading(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
  }) {
    return loading?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$ProfileError value) error,
    required TResult Function(_$ProfileLoading value) loading,
    required TResult Function(_$ProfileStateNotFoundState value) notFound,
    required TResult Function(_$ProfileSuccess value) success,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _$ProfileLoading implements ProfileState {
  const factory _$ProfileLoading({final String? description}) =
      _$_$ProfileLoading;

  String? get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$ProfileLoadingCopyWith<_$_$ProfileLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$ProfileStateNotFoundStateCopyWith<$Res> {
  factory _$$_$ProfileStateNotFoundStateCopyWith(
          _$_$ProfileStateNotFoundState value,
          $Res Function(_$_$ProfileStateNotFoundState) then) =
      __$$_$ProfileStateNotFoundStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_$ProfileStateNotFoundStateCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$$_$ProfileStateNotFoundStateCopyWith<$Res> {
  __$$_$ProfileStateNotFoundStateCopyWithImpl(
      _$_$ProfileStateNotFoundState _value,
      $Res Function(_$_$ProfileStateNotFoundState) _then)
      : super(_value, (v) => _then(v as _$_$ProfileStateNotFoundState));

  @override
  _$_$ProfileStateNotFoundState get _value =>
      super._value as _$_$ProfileStateNotFoundState;
}

/// @nodoc

class _$_$ProfileStateNotFoundState implements _$ProfileStateNotFoundState {
  const _$_$ProfileStateNotFoundState();

  @override
  String toString() {
    return 'ProfileState.notFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$ProfileStateNotFoundState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function(UserModel userModel) success,
  }) {
    return notFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
  }) {
    return notFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$ProfileError value) error,
    required TResult Function(_$ProfileLoading value) loading,
    required TResult Function(_$ProfileStateNotFoundState value) notFound,
    required TResult Function(_$ProfileSuccess value) success,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
  }) {
    return notFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class _$ProfileStateNotFoundState implements ProfileState {
  const factory _$ProfileStateNotFoundState() = _$_$ProfileStateNotFoundState;
}

/// @nodoc
abstract class _$$_$ProfileSuccessCopyWith<$Res> {
  factory _$$_$ProfileSuccessCopyWith(
          _$_$ProfileSuccess value, $Res Function(_$_$ProfileSuccess) then) =
      __$$_$ProfileSuccessCopyWithImpl<$Res>;
  $Res call({UserModel userModel});

  $UserModelCopyWith<$Res> get userModel;
}

/// @nodoc
class __$$_$ProfileSuccessCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$$_$ProfileSuccessCopyWith<$Res> {
  __$$_$ProfileSuccessCopyWithImpl(
      _$_$ProfileSuccess _value, $Res Function(_$_$ProfileSuccess) _then)
      : super(_value, (v) => _then(v as _$_$ProfileSuccess));

  @override
  _$_$ProfileSuccess get _value => super._value as _$_$ProfileSuccess;

  @override
  $Res call({
    Object? userModel = freezed,
  }) {
    return _then(_$_$ProfileSuccess(
      userModel: userModel == freezed
          ? _value.userModel
          : userModel // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ));
  }

  @override
  $UserModelCopyWith<$Res> get userModel {
    return $UserModelCopyWith<$Res>(_value.userModel, (value) {
      return _then(_value.copyWith(userModel: value));
    });
  }
}

/// @nodoc

class _$_$ProfileSuccess implements _$ProfileSuccess {
  const _$_$ProfileSuccess({required this.userModel});

  @override
  final UserModel userModel;

  @override
  String toString() {
    return 'ProfileState.success(userModel: $userModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$ProfileSuccess &&
            const DeepCollectionEquality().equals(other.userModel, userModel));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userModel));

  @JsonKey(ignore: true)
  @override
  _$$_$ProfileSuccessCopyWith<_$_$ProfileSuccess> get copyWith =>
      __$$_$ProfileSuccessCopyWithImpl<_$_$ProfileSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() notFound,
    required TResult Function(UserModel userModel) success,
  }) {
    return success(userModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
  }) {
    return success?.call(userModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? notFound,
    TResult Function(UserModel userModel)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(userModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$ProfileError value) error,
    required TResult Function(_$ProfileLoading value) loading,
    required TResult Function(_$ProfileStateNotFoundState value) notFound,
    required TResult Function(_$ProfileSuccess value) success,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$ProfileError value)? error,
    TResult Function(_$ProfileLoading value)? loading,
    TResult Function(_$ProfileStateNotFoundState value)? notFound,
    TResult Function(_$ProfileSuccess value)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _$ProfileSuccess implements ProfileState {
  const factory _$ProfileSuccess({required final UserModel userModel}) =
      _$_$ProfileSuccess;

  UserModel get userModel => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$ProfileSuccessCopyWith<_$_$ProfileSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}
