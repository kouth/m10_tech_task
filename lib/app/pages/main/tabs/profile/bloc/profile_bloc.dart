import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../../../data/api/local/service/user_service.dart';
import '../../../../../../data/shared/logger/app_logger.dart';
import 'profile_event.dart';
import 'profile_state.dart';

@Injectable()
class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc({
    required this.service,
  }) : super(
          const ProfileState.loading(),
        ) {
    on<ProfileFetchUserEvent>(
      (event, emit) async {
        final _userModel = service.getUserModel();
        if (_userModel != null) {
          emit(
            ProfileState.success(userModel: _userModel),
          );
        } else {
          emit(
            const ProfileState.notFound(),
          );
        }
      },
    );
    on<ProfileClearDatabaseEvent>(
      (event, emit) async {
        try {
          await service.clearDatabase();
        } catch (error, stackTrace) {
          AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
          emit(
            ProfileState.error(
              description: error.toString(),
            ),
          );
        }
      },
    );
    on<ProfileLogOutEvent>(
      (event, emit) {
        service.logout();
      },
    );
  }

  final UserService service;
}
