import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../../../../data/api/local/models/user.dart';

part 'profile_state.freezed.dart';

@freezed
class ProfileState with _$ProfileState {
  const factory ProfileState.error({
    required String description,
  }) = _$ProfileError;

  const factory ProfileState.loading({
    String? description,
  }) = _$ProfileLoading;

  const factory ProfileState.notFound() = _$ProfileStateNotFoundState;

  const factory ProfileState.success({
    required UserModel userModel,
  }) = _$ProfileSuccess;
}
