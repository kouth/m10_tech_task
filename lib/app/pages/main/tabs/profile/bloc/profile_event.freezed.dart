// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'profile_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() clearDatabase,
    required TResult Function() fetchUser,
    required TResult Function() logOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileClearDatabaseEvent value) clearDatabase,
    required TResult Function(ProfileFetchUserEvent value) fetchUser,
    required TResult Function(ProfileLogOutEvent value) logOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileEventCopyWith<$Res> {
  factory $ProfileEventCopyWith(
          ProfileEvent value, $Res Function(ProfileEvent) then) =
      _$ProfileEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProfileEventCopyWithImpl<$Res> implements $ProfileEventCopyWith<$Res> {
  _$ProfileEventCopyWithImpl(this._value, this._then);

  final ProfileEvent _value;
  // ignore: unused_field
  final $Res Function(ProfileEvent) _then;
}

/// @nodoc
abstract class _$$ProfileClearDatabaseEventCopyWith<$Res> {
  factory _$$ProfileClearDatabaseEventCopyWith(
          _$ProfileClearDatabaseEvent value,
          $Res Function(_$ProfileClearDatabaseEvent) then) =
      __$$ProfileClearDatabaseEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileClearDatabaseEventCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$ProfileClearDatabaseEventCopyWith<$Res> {
  __$$ProfileClearDatabaseEventCopyWithImpl(_$ProfileClearDatabaseEvent _value,
      $Res Function(_$ProfileClearDatabaseEvent) _then)
      : super(_value, (v) => _then(v as _$ProfileClearDatabaseEvent));

  @override
  _$ProfileClearDatabaseEvent get _value =>
      super._value as _$ProfileClearDatabaseEvent;
}

/// @nodoc

class _$ProfileClearDatabaseEvent implements ProfileClearDatabaseEvent {
  const _$ProfileClearDatabaseEvent();

  @override
  String toString() {
    return 'ProfileEvent.clearDatabase()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProfileClearDatabaseEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() clearDatabase,
    required TResult Function() fetchUser,
    required TResult Function() logOut,
  }) {
    return clearDatabase();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
  }) {
    return clearDatabase?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
    required TResult orElse(),
  }) {
    if (clearDatabase != null) {
      return clearDatabase();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileClearDatabaseEvent value) clearDatabase,
    required TResult Function(ProfileFetchUserEvent value) fetchUser,
    required TResult Function(ProfileLogOutEvent value) logOut,
  }) {
    return clearDatabase(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
  }) {
    return clearDatabase?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
    required TResult orElse(),
  }) {
    if (clearDatabase != null) {
      return clearDatabase(this);
    }
    return orElse();
  }
}

abstract class ProfileClearDatabaseEvent implements ProfileEvent {
  const factory ProfileClearDatabaseEvent() = _$ProfileClearDatabaseEvent;
}

/// @nodoc
abstract class _$$ProfileFetchUserEventCopyWith<$Res> {
  factory _$$ProfileFetchUserEventCopyWith(_$ProfileFetchUserEvent value,
          $Res Function(_$ProfileFetchUserEvent) then) =
      __$$ProfileFetchUserEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileFetchUserEventCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$ProfileFetchUserEventCopyWith<$Res> {
  __$$ProfileFetchUserEventCopyWithImpl(_$ProfileFetchUserEvent _value,
      $Res Function(_$ProfileFetchUserEvent) _then)
      : super(_value, (v) => _then(v as _$ProfileFetchUserEvent));

  @override
  _$ProfileFetchUserEvent get _value => super._value as _$ProfileFetchUserEvent;
}

/// @nodoc

class _$ProfileFetchUserEvent implements ProfileFetchUserEvent {
  const _$ProfileFetchUserEvent();

  @override
  String toString() {
    return 'ProfileEvent.fetchUser()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileFetchUserEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() clearDatabase,
    required TResult Function() fetchUser,
    required TResult Function() logOut,
  }) {
    return fetchUser();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
  }) {
    return fetchUser?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
    required TResult orElse(),
  }) {
    if (fetchUser != null) {
      return fetchUser();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileClearDatabaseEvent value) clearDatabase,
    required TResult Function(ProfileFetchUserEvent value) fetchUser,
    required TResult Function(ProfileLogOutEvent value) logOut,
  }) {
    return fetchUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
  }) {
    return fetchUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
    required TResult orElse(),
  }) {
    if (fetchUser != null) {
      return fetchUser(this);
    }
    return orElse();
  }
}

abstract class ProfileFetchUserEvent implements ProfileEvent {
  const factory ProfileFetchUserEvent() = _$ProfileFetchUserEvent;
}

/// @nodoc
abstract class _$$ProfileLogOutEventCopyWith<$Res> {
  factory _$$ProfileLogOutEventCopyWith(_$ProfileLogOutEvent value,
          $Res Function(_$ProfileLogOutEvent) then) =
      __$$ProfileLogOutEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileLogOutEventCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$ProfileLogOutEventCopyWith<$Res> {
  __$$ProfileLogOutEventCopyWithImpl(
      _$ProfileLogOutEvent _value, $Res Function(_$ProfileLogOutEvent) _then)
      : super(_value, (v) => _then(v as _$ProfileLogOutEvent));

  @override
  _$ProfileLogOutEvent get _value => super._value as _$ProfileLogOutEvent;
}

/// @nodoc

class _$ProfileLogOutEvent implements ProfileLogOutEvent {
  const _$ProfileLogOutEvent();

  @override
  String toString() {
    return 'ProfileEvent.logOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileLogOutEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() clearDatabase,
    required TResult Function() fetchUser,
    required TResult Function() logOut,
  }) {
    return logOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
  }) {
    return logOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? clearDatabase,
    TResult Function()? fetchUser,
    TResult Function()? logOut,
    required TResult orElse(),
  }) {
    if (logOut != null) {
      return logOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileClearDatabaseEvent value) clearDatabase,
    required TResult Function(ProfileFetchUserEvent value) fetchUser,
    required TResult Function(ProfileLogOutEvent value) logOut,
  }) {
    return logOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
  }) {
    return logOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileClearDatabaseEvent value)? clearDatabase,
    TResult Function(ProfileFetchUserEvent value)? fetchUser,
    TResult Function(ProfileLogOutEvent value)? logOut,
    required TResult orElse(),
  }) {
    if (logOut != null) {
      return logOut(this);
    }
    return orElse();
  }
}

abstract class ProfileLogOutEvent implements ProfileEvent {
  const factory ProfileLogOutEvent() = _$ProfileLogOutEvent;
}
