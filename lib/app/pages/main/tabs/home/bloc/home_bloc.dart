import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../../../data/api/http/models/categories_data_model.dart';
import '../../../../../../data/api/http/repositories/categories_repository.dart';
import '../../../../../../data/shared/logger/app_logger.dart';
import 'home_event.dart';
import 'home_state.dart';

@Injectable()
class HomeViewBloc extends Bloc<HomeViewEvent, HomeViewState> {
  HomeViewBloc({
    required this.repository,
  }) : super(
          const HomeViewState.loading(),
        ) {
    on<HomeViewSummaryEvent>(
      (event, emit) async {
        try {
          emit(const HomeViewState.loading());

          final _result = await repository.getSummary();

          if (_result.data != null) {
            final _categoriesData = CategoriesDataModel.fromJson(_result.data!);
            emit(
              HomeViewState.content(categoriesData: _categoriesData),
            );
          } else {
            emit(
              const HomeViewState.error(description: 'No data'),
            );
          }
        } catch (error, stackTrace) {
          AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
          emit(
            HomeViewState.error(
              description: error.toString(),
            ),
          );
        }
      },
    );
  }

  final CategoriesRepository repository;
}
