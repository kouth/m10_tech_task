import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_event.freezed.dart';

@freezed
class HomeViewEvent with _$HomeViewEvent {
  const factory HomeViewEvent.fetchSummary() = HomeViewSummaryEvent;
}
