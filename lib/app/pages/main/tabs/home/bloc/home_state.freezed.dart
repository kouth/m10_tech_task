// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeViewState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoriesDataModel categoriesData) content,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$HomeViewSummaryContent value) content,
    required TResult Function(_$HomeViewSummaryError value) error,
    required TResult Function(_$HomeViewSummaryLoading value) loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeViewStateCopyWith<$Res> {
  factory $HomeViewStateCopyWith(
          HomeViewState value, $Res Function(HomeViewState) then) =
      _$HomeViewStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewStateCopyWith<$Res> {
  _$HomeViewStateCopyWithImpl(this._value, this._then);

  final HomeViewState _value;
  // ignore: unused_field
  final $Res Function(HomeViewState) _then;
}

/// @nodoc
abstract class _$$_$HomeViewSummaryContentCopyWith<$Res> {
  factory _$$_$HomeViewSummaryContentCopyWith(_$_$HomeViewSummaryContent value,
          $Res Function(_$_$HomeViewSummaryContent) then) =
      __$$_$HomeViewSummaryContentCopyWithImpl<$Res>;
  $Res call({CategoriesDataModel categoriesData});

  $CategoriesDataModelCopyWith<$Res> get categoriesData;
}

/// @nodoc
class __$$_$HomeViewSummaryContentCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements _$$_$HomeViewSummaryContentCopyWith<$Res> {
  __$$_$HomeViewSummaryContentCopyWithImpl(_$_$HomeViewSummaryContent _value,
      $Res Function(_$_$HomeViewSummaryContent) _then)
      : super(_value, (v) => _then(v as _$_$HomeViewSummaryContent));

  @override
  _$_$HomeViewSummaryContent get _value =>
      super._value as _$_$HomeViewSummaryContent;

  @override
  $Res call({
    Object? categoriesData = freezed,
  }) {
    return _then(_$_$HomeViewSummaryContent(
      categoriesData: categoriesData == freezed
          ? _value.categoriesData
          : categoriesData // ignore: cast_nullable_to_non_nullable
              as CategoriesDataModel,
    ));
  }

  @override
  $CategoriesDataModelCopyWith<$Res> get categoriesData {
    return $CategoriesDataModelCopyWith<$Res>(_value.categoriesData, (value) {
      return _then(_value.copyWith(categoriesData: value));
    });
  }
}

/// @nodoc

class _$_$HomeViewSummaryContent implements _$HomeViewSummaryContent {
  const _$_$HomeViewSummaryContent({required this.categoriesData});

  @override
  final CategoriesDataModel categoriesData;

  @override
  String toString() {
    return 'HomeViewState.content(categoriesData: $categoriesData)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$HomeViewSummaryContent &&
            const DeepCollectionEquality()
                .equals(other.categoriesData, categoriesData));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(categoriesData));

  @JsonKey(ignore: true)
  @override
  _$$_$HomeViewSummaryContentCopyWith<_$_$HomeViewSummaryContent>
      get copyWith =>
          __$$_$HomeViewSummaryContentCopyWithImpl<_$_$HomeViewSummaryContent>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoriesDataModel categoriesData) content,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
  }) {
    return content(categoriesData);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
  }) {
    return content?.call(categoriesData);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    required TResult orElse(),
  }) {
    if (content != null) {
      return content(categoriesData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$HomeViewSummaryContent value) content,
    required TResult Function(_$HomeViewSummaryError value) error,
    required TResult Function(_$HomeViewSummaryLoading value) loading,
  }) {
    return content(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
  }) {
    return content?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
    required TResult orElse(),
  }) {
    if (content != null) {
      return content(this);
    }
    return orElse();
  }
}

abstract class _$HomeViewSummaryContent implements HomeViewState {
  const factory _$HomeViewSummaryContent(
          {required final CategoriesDataModel categoriesData}) =
      _$_$HomeViewSummaryContent;

  CategoriesDataModel get categoriesData => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$HomeViewSummaryContentCopyWith<_$_$HomeViewSummaryContent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$HomeViewSummaryErrorCopyWith<$Res> {
  factory _$$_$HomeViewSummaryErrorCopyWith(_$_$HomeViewSummaryError value,
          $Res Function(_$_$HomeViewSummaryError) then) =
      __$$_$HomeViewSummaryErrorCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$$_$HomeViewSummaryErrorCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements _$$_$HomeViewSummaryErrorCopyWith<$Res> {
  __$$_$HomeViewSummaryErrorCopyWithImpl(_$_$HomeViewSummaryError _value,
      $Res Function(_$_$HomeViewSummaryError) _then)
      : super(_value, (v) => _then(v as _$_$HomeViewSummaryError));

  @override
  _$_$HomeViewSummaryError get _value =>
      super._value as _$_$HomeViewSummaryError;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$HomeViewSummaryError(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_$HomeViewSummaryError implements _$HomeViewSummaryError {
  const _$_$HomeViewSummaryError({required this.description});

  @override
  final String description;

  @override
  String toString() {
    return 'HomeViewState.error(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$HomeViewSummaryError &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$HomeViewSummaryErrorCopyWith<_$_$HomeViewSummaryError> get copyWith =>
      __$$_$HomeViewSummaryErrorCopyWithImpl<_$_$HomeViewSummaryError>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoriesDataModel categoriesData) content,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
  }) {
    return error(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
  }) {
    return error?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$HomeViewSummaryContent value) content,
    required TResult Function(_$HomeViewSummaryError value) error,
    required TResult Function(_$HomeViewSummaryLoading value) loading,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _$HomeViewSummaryError implements HomeViewState {
  const factory _$HomeViewSummaryError({required final String description}) =
      _$_$HomeViewSummaryError;

  String get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$HomeViewSummaryErrorCopyWith<_$_$HomeViewSummaryError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$HomeViewSummaryLoadingCopyWith<$Res> {
  factory _$$_$HomeViewSummaryLoadingCopyWith(_$_$HomeViewSummaryLoading value,
          $Res Function(_$_$HomeViewSummaryLoading) then) =
      __$$_$HomeViewSummaryLoadingCopyWithImpl<$Res>;
  $Res call({String? description});
}

/// @nodoc
class __$$_$HomeViewSummaryLoadingCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements _$$_$HomeViewSummaryLoadingCopyWith<$Res> {
  __$$_$HomeViewSummaryLoadingCopyWithImpl(_$_$HomeViewSummaryLoading _value,
      $Res Function(_$_$HomeViewSummaryLoading) _then)
      : super(_value, (v) => _then(v as _$_$HomeViewSummaryLoading));

  @override
  _$_$HomeViewSummaryLoading get _value =>
      super._value as _$_$HomeViewSummaryLoading;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$HomeViewSummaryLoading(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_$HomeViewSummaryLoading implements _$HomeViewSummaryLoading {
  const _$_$HomeViewSummaryLoading({this.description});

  @override
  final String? description;

  @override
  String toString() {
    return 'HomeViewState.loading(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$HomeViewSummaryLoading &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$HomeViewSummaryLoadingCopyWith<_$_$HomeViewSummaryLoading>
      get copyWith =>
          __$$_$HomeViewSummaryLoadingCopyWithImpl<_$_$HomeViewSummaryLoading>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoriesDataModel categoriesData) content,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
  }) {
    return loading(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
  }) {
    return loading?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoriesDataModel categoriesData)? content,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$HomeViewSummaryContent value) content,
    required TResult Function(_$HomeViewSummaryError value) error,
    required TResult Function(_$HomeViewSummaryLoading value) loading,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$HomeViewSummaryContent value)? content,
    TResult Function(_$HomeViewSummaryError value)? error,
    TResult Function(_$HomeViewSummaryLoading value)? loading,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _$HomeViewSummaryLoading implements HomeViewState {
  const factory _$HomeViewSummaryLoading({final String? description}) =
      _$_$HomeViewSummaryLoading;

  String? get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$HomeViewSummaryLoadingCopyWith<_$_$HomeViewSummaryLoading>
      get copyWith => throw _privateConstructorUsedError;
}
