import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../../data/api/http/models/categories_data_model.dart';

part 'home_state.freezed.dart';

@freezed
class HomeViewState with _$HomeViewState {
  const factory HomeViewState.content({
    required CategoriesDataModel categoriesData,
  }) = _$HomeViewSummaryContent;

  const factory HomeViewState.error({
    required String description,
  }) = _$HomeViewSummaryError;

  const factory HomeViewState.loading({
    String? description,
  }) = _$HomeViewSummaryLoading;
}
