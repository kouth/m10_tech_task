import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

import '../../../../utils/config/app_scaffold.dart';
import '../../../../utils/config/navigation/routes.dart';
import '../../../../utils/config/service/l10n/l10n_service.dart';
import '../../../../utils/config/style/app_text_styles.dart';
import '../../../../utils/service/bloc_factory.dart';
import '../../../../widgets/app_bar.dart';
import '../../../../widgets/app_text_tile.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_event.dart';
import 'bloc/home_state.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  static const String routeName = RouteKeys.home;

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>
    with AutomaticKeepAliveClientMixin {
  final _refreshController = RefreshController();

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return AppScaffold(
      child: Scaffold(
        appBar: ApplicationAppBar(
          title: AppLocale.of(context).home,
          centerTitle: true,
        ),
        body: SafeArea(
          child: BlocProvider(
            create: (BuildContext context) {
              return context.read<BlocFactory>().create<HomeViewBloc>()
                ..add(
                  const HomeViewSummaryEvent(),
                );
            },
            child: BlocBuilder<HomeViewBloc, HomeViewState>(
              builder: (context, state) {
                return Center(
                  child: state.when(
                    content: (
                      data,
                    ) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16),
                            child: Text(
                              AppLocale.of(context).itemCount(data.count),
                              style: AppTextStyle.h5Regular(context),
                            ),
                          ),
                          const Divider(
                            height: 1,
                            thickness: 2,
                          ),
                          Expanded(
                            child: SmartRefresher(
                              controller: _refreshController,
                              onRefresh: () {
                                context.read<HomeViewBloc>().add(
                                      const HomeViewSummaryEvent(),
                                    );
                                _refreshController.refreshCompleted();
                              },
                              child: ListView.separated(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                itemCount: data.categories.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    child: AppTextTileWidget(
                                      title: '${index + 1}',
                                      value: data.categories.elementAt(index),
                                    ),
                                  );
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return const SizedBox(
                                    height: 16,
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                    error: (description) {
                      return Center(
                        child: TextButton(
                          onPressed: () {
                            context.read<HomeViewBloc>().add(
                                  const HomeViewSummaryEvent(),
                                );
                          },
                          child: Text(
                            AppLocale.of(context).errorWithMessage(description),
                          ),
                        ),
                      );
                    },
                    loading: (description) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppLocale.of(context).loading,
                          ),
                          const Padding(
                            padding: EdgeInsets.all(16),
                            child: LinearProgressIndicator(),
                          ),
                        ],
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
