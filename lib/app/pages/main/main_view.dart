import 'package:flutter/material.dart';

import '../../utils/config/app_scaffold.dart';
import '../../utils/config/navigation/routes.dart';
import '../../utils/config/service/l10n/l10n_service.dart';
import '../../utils/config/style/app_colors.dart';
import '../../utils/config/style/app_text_styles.dart';
import 'tabs/home/home_view.dart';
import 'tabs/profile/profile_view.dart';

class MainView extends StatefulWidget {
  const MainView({Key? key}) : super(key: key);

  static const String routeName = RouteKeys.main;

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      vsync: this,
      length: 2,
      animationDuration: Duration.zero,
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      child: Scaffold(
        body: TabBarView(
          physics: const NeverScrollableScrollPhysics(),
          controller: tabController,
          children: const [
            HomeView(),
            ProfileView(),
          ],
        ),
        bottomNavigationBar: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Divider(
                height: 1,
                thickness: 2,
              ),
              TabBar(
                labelColor: AppColors.black,
                labelPadding: EdgeInsets.zero,
                labelStyle: AppTextStyle.h5Regular(context),
                controller: tabController,
                unselectedLabelColor: AppColors.black,
                tabs: [
                  AppLocale.of(context).home,
                  AppLocale.of(context).profile,
                ].map(
                  (data) {
                    return Tab(
                      iconMargin: const EdgeInsets.only(bottom: 4),
                      text: data,
                    );
                  },
                ).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
