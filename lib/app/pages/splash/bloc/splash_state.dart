import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../data/api/local/models/user.dart';

part 'splash_state.freezed.dart';

@freezed
class SplashViewState with _$SplashViewState {
  const factory SplashViewState.authorized({
    required UserModel? userModel,
  }) = _$SplashViewAuthorizedState;

  const factory SplashViewState.error({
    required String description,
  }) = _$SplashViewSummaryErrorState;

  const factory SplashViewState.loading({
    String? description,
  }) = _$SplashViewSummaryLoadingState;

  const factory SplashViewState.unAuthorized() = _$SplashViewUnAuthorizedState;
}
