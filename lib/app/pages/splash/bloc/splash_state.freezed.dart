// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'splash_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SplashViewState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel? userModel) authorized,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() unAuthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SplashViewAuthorizedState value) authorized,
    required TResult Function(_$SplashViewSummaryErrorState value) error,
    required TResult Function(_$SplashViewSummaryLoadingState value) loading,
    required TResult Function(_$SplashViewUnAuthorizedState value) unAuthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SplashViewStateCopyWith<$Res> {
  factory $SplashViewStateCopyWith(
          SplashViewState value, $Res Function(SplashViewState) then) =
      _$SplashViewStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SplashViewStateCopyWithImpl<$Res>
    implements $SplashViewStateCopyWith<$Res> {
  _$SplashViewStateCopyWithImpl(this._value, this._then);

  final SplashViewState _value;
  // ignore: unused_field
  final $Res Function(SplashViewState) _then;
}

/// @nodoc
abstract class _$$_$SplashViewAuthorizedStateCopyWith<$Res> {
  factory _$$_$SplashViewAuthorizedStateCopyWith(
          _$_$SplashViewAuthorizedState value,
          $Res Function(_$_$SplashViewAuthorizedState) then) =
      __$$_$SplashViewAuthorizedStateCopyWithImpl<$Res>;
  $Res call({UserModel? userModel});

  $UserModelCopyWith<$Res>? get userModel;
}

/// @nodoc
class __$$_$SplashViewAuthorizedStateCopyWithImpl<$Res>
    extends _$SplashViewStateCopyWithImpl<$Res>
    implements _$$_$SplashViewAuthorizedStateCopyWith<$Res> {
  __$$_$SplashViewAuthorizedStateCopyWithImpl(
      _$_$SplashViewAuthorizedState _value,
      $Res Function(_$_$SplashViewAuthorizedState) _then)
      : super(_value, (v) => _then(v as _$_$SplashViewAuthorizedState));

  @override
  _$_$SplashViewAuthorizedState get _value =>
      super._value as _$_$SplashViewAuthorizedState;

  @override
  $Res call({
    Object? userModel = freezed,
  }) {
    return _then(_$_$SplashViewAuthorizedState(
      userModel: userModel == freezed
          ? _value.userModel
          : userModel // ignore: cast_nullable_to_non_nullable
              as UserModel?,
    ));
  }

  @override
  $UserModelCopyWith<$Res>? get userModel {
    if (_value.userModel == null) {
      return null;
    }

    return $UserModelCopyWith<$Res>(_value.userModel!, (value) {
      return _then(_value.copyWith(userModel: value));
    });
  }
}

/// @nodoc

class _$_$SplashViewAuthorizedState implements _$SplashViewAuthorizedState {
  const _$_$SplashViewAuthorizedState({required this.userModel});

  @override
  final UserModel? userModel;

  @override
  String toString() {
    return 'SplashViewState.authorized(userModel: $userModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SplashViewAuthorizedState &&
            const DeepCollectionEquality().equals(other.userModel, userModel));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userModel));

  @JsonKey(ignore: true)
  @override
  _$$_$SplashViewAuthorizedStateCopyWith<_$_$SplashViewAuthorizedState>
      get copyWith => __$$_$SplashViewAuthorizedStateCopyWithImpl<
          _$_$SplashViewAuthorizedState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel? userModel) authorized,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() unAuthorized,
  }) {
    return authorized(userModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
  }) {
    return authorized?.call(userModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
    required TResult orElse(),
  }) {
    if (authorized != null) {
      return authorized(userModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SplashViewAuthorizedState value) authorized,
    required TResult Function(_$SplashViewSummaryErrorState value) error,
    required TResult Function(_$SplashViewSummaryLoadingState value) loading,
    required TResult Function(_$SplashViewUnAuthorizedState value) unAuthorized,
  }) {
    return authorized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
  }) {
    return authorized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
    required TResult orElse(),
  }) {
    if (authorized != null) {
      return authorized(this);
    }
    return orElse();
  }
}

abstract class _$SplashViewAuthorizedState implements SplashViewState {
  const factory _$SplashViewAuthorizedState(
      {required final UserModel? userModel}) = _$_$SplashViewAuthorizedState;

  UserModel? get userModel => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SplashViewAuthorizedStateCopyWith<_$_$SplashViewAuthorizedState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SplashViewSummaryErrorStateCopyWith<$Res> {
  factory _$$_$SplashViewSummaryErrorStateCopyWith(
          _$_$SplashViewSummaryErrorState value,
          $Res Function(_$_$SplashViewSummaryErrorState) then) =
      __$$_$SplashViewSummaryErrorStateCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$$_$SplashViewSummaryErrorStateCopyWithImpl<$Res>
    extends _$SplashViewStateCopyWithImpl<$Res>
    implements _$$_$SplashViewSummaryErrorStateCopyWith<$Res> {
  __$$_$SplashViewSummaryErrorStateCopyWithImpl(
      _$_$SplashViewSummaryErrorState _value,
      $Res Function(_$_$SplashViewSummaryErrorState) _then)
      : super(_value, (v) => _then(v as _$_$SplashViewSummaryErrorState));

  @override
  _$_$SplashViewSummaryErrorState get _value =>
      super._value as _$_$SplashViewSummaryErrorState;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$SplashViewSummaryErrorState(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_$SplashViewSummaryErrorState implements _$SplashViewSummaryErrorState {
  const _$_$SplashViewSummaryErrorState({required this.description});

  @override
  final String description;

  @override
  String toString() {
    return 'SplashViewState.error(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SplashViewSummaryErrorState &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$SplashViewSummaryErrorStateCopyWith<_$_$SplashViewSummaryErrorState>
      get copyWith => __$$_$SplashViewSummaryErrorStateCopyWithImpl<
          _$_$SplashViewSummaryErrorState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel? userModel) authorized,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() unAuthorized,
  }) {
    return error(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
  }) {
    return error?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SplashViewAuthorizedState value) authorized,
    required TResult Function(_$SplashViewSummaryErrorState value) error,
    required TResult Function(_$SplashViewSummaryLoadingState value) loading,
    required TResult Function(_$SplashViewUnAuthorizedState value) unAuthorized,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _$SplashViewSummaryErrorState implements SplashViewState {
  const factory _$SplashViewSummaryErrorState(
      {required final String description}) = _$_$SplashViewSummaryErrorState;

  String get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SplashViewSummaryErrorStateCopyWith<_$_$SplashViewSummaryErrorState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SplashViewSummaryLoadingStateCopyWith<$Res> {
  factory _$$_$SplashViewSummaryLoadingStateCopyWith(
          _$_$SplashViewSummaryLoadingState value,
          $Res Function(_$_$SplashViewSummaryLoadingState) then) =
      __$$_$SplashViewSummaryLoadingStateCopyWithImpl<$Res>;
  $Res call({String? description});
}

/// @nodoc
class __$$_$SplashViewSummaryLoadingStateCopyWithImpl<$Res>
    extends _$SplashViewStateCopyWithImpl<$Res>
    implements _$$_$SplashViewSummaryLoadingStateCopyWith<$Res> {
  __$$_$SplashViewSummaryLoadingStateCopyWithImpl(
      _$_$SplashViewSummaryLoadingState _value,
      $Res Function(_$_$SplashViewSummaryLoadingState) _then)
      : super(_value, (v) => _then(v as _$_$SplashViewSummaryLoadingState));

  @override
  _$_$SplashViewSummaryLoadingState get _value =>
      super._value as _$_$SplashViewSummaryLoadingState;

  @override
  $Res call({
    Object? description = freezed,
  }) {
    return _then(_$_$SplashViewSummaryLoadingState(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_$SplashViewSummaryLoadingState
    implements _$SplashViewSummaryLoadingState {
  const _$_$SplashViewSummaryLoadingState({this.description});

  @override
  final String? description;

  @override
  String toString() {
    return 'SplashViewState.loading(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SplashViewSummaryLoadingState &&
            const DeepCollectionEquality()
                .equals(other.description, description));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(description));

  @JsonKey(ignore: true)
  @override
  _$$_$SplashViewSummaryLoadingStateCopyWith<_$_$SplashViewSummaryLoadingState>
      get copyWith => __$$_$SplashViewSummaryLoadingStateCopyWithImpl<
          _$_$SplashViewSummaryLoadingState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel? userModel) authorized,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() unAuthorized,
  }) {
    return loading(description);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
  }) {
    return loading?.call(description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SplashViewAuthorizedState value) authorized,
    required TResult Function(_$SplashViewSummaryErrorState value) error,
    required TResult Function(_$SplashViewSummaryLoadingState value) loading,
    required TResult Function(_$SplashViewUnAuthorizedState value) unAuthorized,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _$SplashViewSummaryLoadingState implements SplashViewState {
  const factory _$SplashViewSummaryLoadingState({final String? description}) =
      _$_$SplashViewSummaryLoadingState;

  String? get description => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_$SplashViewSummaryLoadingStateCopyWith<_$_$SplashViewSummaryLoadingState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_$SplashViewUnAuthorizedStateCopyWith<$Res> {
  factory _$$_$SplashViewUnAuthorizedStateCopyWith(
          _$_$SplashViewUnAuthorizedState value,
          $Res Function(_$_$SplashViewUnAuthorizedState) then) =
      __$$_$SplashViewUnAuthorizedStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_$SplashViewUnAuthorizedStateCopyWithImpl<$Res>
    extends _$SplashViewStateCopyWithImpl<$Res>
    implements _$$_$SplashViewUnAuthorizedStateCopyWith<$Res> {
  __$$_$SplashViewUnAuthorizedStateCopyWithImpl(
      _$_$SplashViewUnAuthorizedState _value,
      $Res Function(_$_$SplashViewUnAuthorizedState) _then)
      : super(_value, (v) => _then(v as _$_$SplashViewUnAuthorizedState));

  @override
  _$_$SplashViewUnAuthorizedState get _value =>
      super._value as _$_$SplashViewUnAuthorizedState;
}

/// @nodoc

class _$_$SplashViewUnAuthorizedState implements _$SplashViewUnAuthorizedState {
  const _$_$SplashViewUnAuthorizedState();

  @override
  String toString() {
    return 'SplashViewState.unAuthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_$SplashViewUnAuthorizedState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UserModel? userModel) authorized,
    required TResult Function(String description) error,
    required TResult Function(String? description) loading,
    required TResult Function() unAuthorized,
  }) {
    return unAuthorized();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
  }) {
    return unAuthorized?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UserModel? userModel)? authorized,
    TResult Function(String description)? error,
    TResult Function(String? description)? loading,
    TResult Function()? unAuthorized,
    required TResult orElse(),
  }) {
    if (unAuthorized != null) {
      return unAuthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_$SplashViewAuthorizedState value) authorized,
    required TResult Function(_$SplashViewSummaryErrorState value) error,
    required TResult Function(_$SplashViewSummaryLoadingState value) loading,
    required TResult Function(_$SplashViewUnAuthorizedState value) unAuthorized,
  }) {
    return unAuthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
  }) {
    return unAuthorized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_$SplashViewAuthorizedState value)? authorized,
    TResult Function(_$SplashViewSummaryErrorState value)? error,
    TResult Function(_$SplashViewSummaryLoadingState value)? loading,
    TResult Function(_$SplashViewUnAuthorizedState value)? unAuthorized,
    required TResult orElse(),
  }) {
    if (unAuthorized != null) {
      return unAuthorized(this);
    }
    return orElse();
  }
}

abstract class _$SplashViewUnAuthorizedState implements SplashViewState {
  const factory _$SplashViewUnAuthorizedState() =
      _$_$SplashViewUnAuthorizedState;
}
