import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../data/api/local/service/user_service.dart';
import '../../../../data/shared/logger/app_logger.dart';
import 'splash_event.dart';
import 'splash_state.dart';

@Injectable()
class SplashViewBloc extends Bloc<SplashViewEvent, SplashViewState> {
  SplashViewBloc({
    required this.service,
  }) : super(
          const SplashViewState.loading(),
        ) {
    on<SplashViewSummaryEvent>(
      (event, emit) async {
        try {
          final userModel = service.getUserModel();
          if (userModel != null) {
            emit(
              SplashViewState.authorized(userModel: userModel),
            );
          } else {
            emit(
              const SplashViewState.unAuthorized(),
            );
          }
        } catch (error, stackTrace) {
          AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
          emit(
            SplashViewState.error(
              description: error.toString(),
            ),
          );
        }
      },
    );
  }

  final UserService service;
}
