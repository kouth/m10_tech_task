import 'package:freezed_annotation/freezed_annotation.dart';

part 'splash_event.freezed.dart';

@freezed
class SplashViewEvent with _$SplashViewEvent {
  const factory SplashViewEvent.fetchSummary() = SplashViewSummaryEvent;
}
