import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../utils/config/navigation/routes.dart';
import '../../utils/config/service/l10n/l10n_service.dart';
import '../../utils/service/bloc_factory.dart';
import '../auth/sign_in/sign_in_view.dart';
import '../main/main_view.dart';
import 'bloc/splash_bloc.dart';
import 'bloc/splash_event.dart';
import 'bloc/splash_state.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  static const String routeName = RouteKeys.splash;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return context.read<BlocFactory>().create<SplashViewBloc>()
          ..add(
            const SplashViewSummaryEvent(),
          );
      },
      child: BlocBuilder<SplashViewBloc, SplashViewState>(
        builder: (context, state) {
          return state.when(
            authorized: (userModel) {
              return const MainView();
            },
            unAuthorized: () {
              return const SignInView();
            },
            error: (description) {
              return Scaffold(
                body: Center(
                  child: Text(
                    AppLocale.of(context).errorWithMessage(description),
                  ),
                ),
              );
            },
            loading: (description) {
              return Scaffold(
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      AppLocale.of(context).loading,
                    ),
                    const Padding(
                      padding: EdgeInsets.all(16),
                      child: LinearProgressIndicator(),
                    ),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
