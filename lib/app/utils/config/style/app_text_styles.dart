import 'package:flutter/material.dart';

class AppTextStyle {
  static TextStyle? h5Bold(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1?.copyWith(
          fontSize: 12,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle? h5Regular(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1?.copyWith(
          fontSize: 12,
        );
  }

  static TextStyle? h4Bold(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1?.copyWith(
          fontSize: 16,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle? h4Regular(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1?.copyWith(
          fontSize: 16,
        );
  }
}
