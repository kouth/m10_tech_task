import 'package:flutter/material.dart';

import '../../../../../data/shared/l10n/generated/l10n.dart';

class AppLocale {
  const AppLocale();

  static List<Locale> get supportedLocales => S.delegate.supportedLocales;

  static AppLocalizationDelegate get delegate => S.delegate;

  static S get current {
    return S.current;
  }

  static S of(BuildContext context) {
    return S.of(context);
  }
}
