// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i7;

import '../../../../data/api/http/app_dio.dart' as _i3;
import '../../../../data/api/http/repositories/categories_repository.dart'
    as _i4;
import '../../../../data/api/local/service/user_service.dart' as _i10;
import '../../../../data/shared/l10n/language/language_controller.dart' as _i9;
import '../../../../data/shared/l10n/language/language_service.dart' as _i6;
import '../../../../data/shared/storage/storage_service.dart' as _i8;
import '../../../pages/auth/sign_in/bloc/sign_in_bloc.dart' as _i12;
import '../../../pages/auth/sign_up/bloc/sign_up_bloc.dart' as _i13;
import '../../../pages/main/tabs/home/bloc/home_bloc.dart' as _i5;
import '../../../pages/main/tabs/profile/bloc/profile_bloc.dart' as _i11;
import '../../../pages/splash/bloc/splash_bloc.dart' as _i14;
import 'modules/register_module.dart'
    as _i15; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.factory<_i3.AppDio>(() => _i3.AppDio());
  gh.factory<_i4.CategoriesRepository>(
      () => _i4.CategoriesRepository(get<_i3.AppDio>()));
  gh.factory<_i5.HomeViewBloc>(
      () => _i5.HomeViewBloc(repository: get<_i4.CategoriesRepository>()));
  gh.singleton<_i6.LocaleSettingsService>(_i6.LocaleSettingsService());
  await gh.factoryAsync<_i7.SharedPreferences>(() => registerModule.prefs,
      preResolve: true);
  gh.factory<_i8.AppStorageService>(
      () => _i8.AppStorageService(get<_i7.SharedPreferences>()));
  gh.singleton<_i9.LocaleSettingsController>(
      _i9.LocaleSettingsController(get<_i6.LocaleSettingsService>()));
  gh.factory<_i10.UserService>(
      () => _i10.UserService(appStorageService: get<_i8.AppStorageService>()));
  gh.factory<_i11.ProfileBloc>(
      () => _i11.ProfileBloc(service: get<_i10.UserService>()));
  gh.factory<_i12.SignInBloc>(
      () => _i12.SignInBloc(service: get<_i10.UserService>()));
  gh.factory<_i13.SignUpBloc>(
      () => _i13.SignUpBloc(service: get<_i10.UserService>()));
  gh.factory<_i14.SplashViewBloc>(
      () => _i14.SplashViewBloc(service: get<_i10.UserService>()));
  return get;
}

class _$RegisterModule extends _i15.RegisterModule {}
