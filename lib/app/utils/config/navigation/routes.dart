class RouteKeys {
  static const String home = '${main}home/';
  static const String main = '${splash}main/';
  static const String profile = '${main}profile/';
  static const String routeNotFound = '';
  static const String signIn = '${splash}sign_in/';
  static const String signUp = '${splash}sign_up/';
  static const String splash = 'splash/';
}
