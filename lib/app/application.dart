import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import '../data/shared/l10n/language/language_controller.dart';
import 'pages/auth/sign_in/sign_in_view.dart';
import 'pages/auth/sign_up/sign_up_view.dart';
import 'pages/main/main_view.dart';
import 'pages/shared/no_route/no_route_view.dart';
import 'pages/splash/splash_view.dart';
import 'utils/config/scroll_behavior.dart';
import 'utils/config/service/l10n/l10n_service.dart';
import 'utils/service/bloc_factory.dart';

class Application extends StatelessWidget {
  const Application({
    Key? key,
  }) : super(key: key);

  Route<dynamic>? _onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SignInView.routeName:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const SignInView(),
        );
      case SignUpView.routeName:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const SignUpView(),
        );
      case SplashView.routeName:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const SplashView(),
        );
      case MainView.routeName:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const MainView(),
        );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => NoRouteView(
            text: settings.name.toString(),
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) => BlocFactory(getIt: GetIt.instance),
        ),
        ChangeNotifierProvider(
          create: (context) => GetIt.instance.get<LocaleSettingsController>(),
        ),
      ],
      child: Builder(
        builder: (context) {
          return AnimatedBuilder(
            animation: context.read<LocaleSettingsController>(),
            builder: (BuildContext context, Widget? child) {
              return MaterialApp(
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                ),
                scrollBehavior: AppScrollBehavior(),
                locale: context.read<LocaleSettingsController>().locale,
                onGenerateRoute: _onGenerateRoute,
                localizationsDelegates: [
                  AppLocale.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                supportedLocales: AppLocale.supportedLocales,
                initialRoute: SplashView.routeName,
              );
            },
          );
        },
      ),
    );
  }
}
