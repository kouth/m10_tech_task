import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utils/config/style/app_colors.dart';
import '../utils/config/style/app_text_styles.dart';

class ApplicationAppBar extends StatelessWidget with PreferredSizeWidget {
  const ApplicationAppBar({
    Key? key,
    this.actions,
    this.child,
    this.leading,
    this.centerTitle = false,
    this.backgroundColor,
    this.automaticallyImplyLeading = true,
    this.iconColor,
    this.textColor = AppColors.white,
    this.title,
    this.bottom,
    this.brightness = Brightness.light,
    this.leadingWidth,
    this.elevation = 0.0,
    this.shape,
    this.size,
  })  : assert(
          child == null || title == null,
          'Cannot provide both a title and a child',
        ),
        super(key: key);

  final List<Widget>? actions;
  final bool automaticallyImplyLeading;
  final Color? backgroundColor;
  final PreferredSizeWidget? bottom;
  final Brightness? brightness;
  final bool centerTitle;
  final Widget? child;
  final double? elevation;
  final Color? iconColor;
  final Widget? leading;
  final double? leadingWidth;
  final ShapeBorder? shape;
  final Size? size;
  final Color? textColor;
  final String? title;

  @override
  Size get preferredSize =>
      size ??
      (Platform.isIOS
          ? const CupertinoNavigationBar().preferredSize
          : AppBar().preferredSize);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: backgroundColor,
      centerTitle: centerTitle,
      systemOverlayStyle:
          Theme.of(context).appBarTheme.systemOverlayStyle?.copyWith(
                statusBarBrightness: brightness,
                statusBarIconBrightness: brightness,
                systemNavigationBarIconBrightness: brightness,
              ),
      leadingWidth: leadingWidth,
      iconTheme: Theme.of(context).appBarTheme.iconTheme?.copyWith(
            color: iconColor,
          ),
      shape: shape,
      elevation: elevation,
      automaticallyImplyLeading: automaticallyImplyLeading,
      actions: actions,
      leading: leading,
      bottom: bottom,
      title: child ??
          Text(
            title ?? '',
            style: AppTextStyle.h5Bold(context)?.copyWith(
              color: textColor,
            ),
          ),
    );
  }
}
