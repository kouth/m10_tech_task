import 'package:flutter/material.dart';

class AppTextFieldWidget extends StatelessWidget {
  const AppTextFieldWidget({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.labelText,
    this.readOnly = false,
    this.obscureText = false,
    this.onTap,
  }) : super(key: key);

  final TextEditingController controller;
  final String hintText;
  final String labelText;
  final bool obscureText;
  final VoidCallback? onTap;
  final bool readOnly;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: onTap,
      obscureText: obscureText,
      controller: controller,
      readOnly: readOnly,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        hintText: hintText,
        labelText: labelText,
      ),
    );
  }
}
