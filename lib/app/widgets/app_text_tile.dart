import 'dart:ui';

import 'package:flutter/material.dart';

import '../utils/config/style/app_text_styles.dart';

class AppTextTileWidget extends StatelessWidget {
  const AppTextTileWidget({
    Key? key,
    required this.title,
    required this.value,
    this.withDivider = true,
  }) : super(key: key);

  final String title;
  final String value;
  final bool withDivider;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: DefaultTextStyle.of(context).style,
        children: <TextSpan>[
          TextSpan(
            text: title,
            style: AppTextStyle.h5Bold(context)?.copyWith(
              fontFeatures: const [FontFeature.tabularFigures()],
            ),
          ),
          if (withDivider) const TextSpan(text: ': '),
          TextSpan(
            text: value,
            style: AppTextStyle.h5Regular(context),
          ),
        ],
      ),
    );
  }
}
