import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:injectable/injectable.dart';

import '../../../../app/utils/config/constants/storage_keys.dart';
import '../../../shared/logger/app_logger.dart';
import '../../../shared/storage/storage_service.dart';
import '../models/user.dart';

@injectable
class UserService {
  UserService({
    required this.appStorageService,
  });

  final AppStorageService appStorageService;

  Future<bool> clearDatabase() async {
    return appStorageService.clear();
  }

  Future<bool> logout() async {
    try {
      return appStorageService.remove(key: StorageKeys.user);
    } catch (error, stackTrace) {
      AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
      Error.throwWithStackTrace(error, stackTrace);
    }
  }

  UserModel? getUserModel() {
    try {
      final _userString = appStorageService.getString(
        key: StorageKeys.user,
      );
      if (_userString != null) {
        final _userJson = jsonDecode(_userString) as Map<String, dynamic>;
        final _userModel = UserModel.fromJson(_userJson);
        return _userModel;
      }
      return null;
    } catch (error, stackTrace) {
      AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
      Error.throwWithStackTrace(error, stackTrace);
    }
  }

  Future<UserModel?> signIn({
    required String login,
    required String password,
  }) async {
    try {
      final _passwordBytes = utf8.encode(password);
      final _sha256password = sha256.convert(_passwordBytes);
      final _userString = appStorageService.getString(
        key: login.toLowerCase(),
      );
      if (_userString != null) {
        final _userJson = jsonDecode(_userString) as Map<String, dynamic>;
        final _userModel = UserModel.fromJson(_userJson);
        if (_userModel.password == _sha256password.toString()) {
          await appStorageService.setString(
            key: StorageKeys.user,
            value: jsonEncode(_userJson),
          );
          return _userModel;
        }
      }
      return null;
    } catch (error, stackTrace) {
      AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
      Error.throwWithStackTrace(error, stackTrace);
    }
  }

  Future<UserModel?> signUp({
    required String name,
    required String login,
    required String password,
    required DateTime date,
  }) async {
    try {
      if (appStorageService.getString(key: login) != null) return null;

      final _passwordBytes = utf8.encode(password);
      final _sha256password = sha256.convert(_passwordBytes);

      final _userModel = UserModel(
        name: name,
        login: login,
        password: _sha256password.toString(),
        date: date,
      );

      await appStorageService.setString(
        key: login.toLowerCase(),
        value: jsonEncode(
          _userModel.toJson(),
        ),
      );
      await appStorageService.setString(
        key: StorageKeys.user,
        value: jsonEncode(_userModel),
      );

      return _userModel;
    } catch (error, stackTrace) {
      AppLogger.logger.e('Error is $error, stackTrace $stackTrace');
      Error.throwWithStackTrace(error, stackTrace);
    }
  }
}
