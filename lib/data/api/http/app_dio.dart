import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../../app/utils/config/constants/api_constants.dart';

@Injectable()
class AppDio {
  static final BaseOptions _options = BaseOptions(
    baseUrl: ApiCosntants.host,
    connectTimeout: 30000,
    receiveTimeout: 60000,
    followRedirects: false,
    validateStatus: (status) {
      return status! < 400;
    },
  );

  final Dio _dio = Dio(
    _options,
  )..interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
      ),
    );

  Future<Response<dynamic>?> delete(
    String path, {
    dynamic data,
    Map<String, dynamic>? headers,
    void Function(int, int)? onSendProgress,
    void Function(int, int)? onReceiveProgress,
  }) async {
    try {
      if (headers != null) {
        _dio.options.headers.addAll(headers);
      }
      return await _dio.delete(
        path,
        data: data,
      );
    } on DioError catch (e, stackTrace) {
      if (e.response != null) {
        return e.response;
      } else {
        Error.throwWithStackTrace(
          'Failed to delete: ${e.message}',
          stackTrace,
        );
      }
    } catch (e, stackTrace) {
      Error.throwWithStackTrace(
        'Failed to send request',
        stackTrace,
      );
    }
  }

  Future<Response<dynamic>?> put(
    String path, {
    dynamic data,
    Map<String, dynamic>? headers,
    void Function(int, int)? onSendProgress,
    void Function(int, int)? onReceiveProgress,
  }) async {
    try {
      if (headers != null) {
        _dio.options.headers.addAll(headers);
      }
      return await _dio.put(
        path,
        data: data,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
    } on DioError catch (e, stackTrace) {
      if (e.response != null) {
        return e.response;
      } else {
        Error.throwWithStackTrace(
          'Failed to put: ${e.message}',
          stackTrace,
        );
      }
    } catch (e, stackTrace) {
      Error.throwWithStackTrace(
        'Failed to send request',
        stackTrace,
      );
    }
  }

  Future<Response<dynamic>?> post(
    String path, {
    dynamic data,
    Map<String, dynamic>? headers,
    void Function(int, int)? onSendProgress,
    void Function(int, int)? onReceiveProgress,
  }) async {
    try {
      if (headers != null) {
        _dio.options.headers.addAll(headers);
      }
      return await _dio.post(
        path,
        data: data,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
    } on DioError catch (e, stackTrace) {
      if (e.response != null) {
        return e.response;
      } else {
        Error.throwWithStackTrace(
          'Failed to post: ${e.message}',
          stackTrace,
        );
      }
    } catch (e, stackTrace) {
      Error.throwWithStackTrace(
        'Failed to send request',
        stackTrace,
      );
    }
  }

  Future<Response<dynamic>?> getRequest(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    void Function(int, int)? onReceiveProgress,
    Options? options,
  }) async {
    try {
      if (headers != null) {
        _dio.options.headers.addAll(headers);
      }
      return await _dio.get(
        path,
        queryParameters: queryParameters,
        onReceiveProgress: onReceiveProgress,
        options: options,
      );
    } on DioError catch (e, stackTrace) {
      if (e.response != null) {
        return e.response;
      } else {
        Error.throwWithStackTrace(
          'Failed to get: ${e.message}',
          stackTrace,
        );
      }
    } catch (e, stackTrace) {
      Error.throwWithStackTrace(
        'Failed to send request',
        stackTrace,
      );
    }
  }

  Future<Response<dynamic>?> download({
    required String url,
  }) async {
    try {
      final response = await _dio.get(
        url,
        options: Options(
          responseType: ResponseType.bytes,
        ),
      );
      return response;
    } on DioError catch (e, stackTrace) {
      if (e.response != null) {
        return e.response;
      } else {
        Error.throwWithStackTrace(
          'Failed to download: ${e.message}',
          stackTrace,
        );
      }
    } catch (e, stackTrace) {
      Error.throwWithStackTrace(
        'Failed to send request',
        stackTrace,
      );
    }
  }
}
