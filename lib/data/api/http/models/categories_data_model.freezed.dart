// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'categories_data_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CategoriesDataModel _$CategoriesDataModelFromJson(Map<String, dynamic> json) {
  return _CategoriesDataModel.fromJson(json);
}

/// @nodoc
mixin _$CategoriesDataModel {
  List<String> get categories => throw _privateConstructorUsedError;
  int get count => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CategoriesDataModelCopyWith<CategoriesDataModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoriesDataModelCopyWith<$Res> {
  factory $CategoriesDataModelCopyWith(
          CategoriesDataModel value, $Res Function(CategoriesDataModel) then) =
      _$CategoriesDataModelCopyWithImpl<$Res>;
  $Res call({List<String> categories, int count});
}

/// @nodoc
class _$CategoriesDataModelCopyWithImpl<$Res>
    implements $CategoriesDataModelCopyWith<$Res> {
  _$CategoriesDataModelCopyWithImpl(this._value, this._then);

  final CategoriesDataModel _value;
  // ignore: unused_field
  final $Res Function(CategoriesDataModel) _then;

  @override
  $Res call({
    Object? categories = freezed,
    Object? count = freezed,
  }) {
    return _then(_value.copyWith(
      categories: categories == freezed
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<String>,
      count: count == freezed
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_CategoriesDataModelCopyWith<$Res>
    implements $CategoriesDataModelCopyWith<$Res> {
  factory _$$_CategoriesDataModelCopyWith(_$_CategoriesDataModel value,
          $Res Function(_$_CategoriesDataModel) then) =
      __$$_CategoriesDataModelCopyWithImpl<$Res>;
  @override
  $Res call({List<String> categories, int count});
}

/// @nodoc
class __$$_CategoriesDataModelCopyWithImpl<$Res>
    extends _$CategoriesDataModelCopyWithImpl<$Res>
    implements _$$_CategoriesDataModelCopyWith<$Res> {
  __$$_CategoriesDataModelCopyWithImpl(_$_CategoriesDataModel _value,
      $Res Function(_$_CategoriesDataModel) _then)
      : super(_value, (v) => _then(v as _$_CategoriesDataModel));

  @override
  _$_CategoriesDataModel get _value => super._value as _$_CategoriesDataModel;

  @override
  $Res call({
    Object? categories = freezed,
    Object? count = freezed,
  }) {
    return _then(_$_CategoriesDataModel(
      categories: categories == freezed
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<String>,
      count: count == freezed
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CategoriesDataModel implements _CategoriesDataModel {
  const _$_CategoriesDataModel(
      {required final List<String> categories, required this.count})
      : _categories = categories;

  factory _$_CategoriesDataModel.fromJson(Map<String, dynamic> json) =>
      _$$_CategoriesDataModelFromJson(json);

  final List<String> _categories;
  @override
  List<String> get categories {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_categories);
  }

  @override
  final int count;

  @override
  String toString() {
    return 'CategoriesDataModel(categories: $categories, count: $count)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CategoriesDataModel &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories) &&
            const DeepCollectionEquality().equals(other.count, count));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_categories),
      const DeepCollectionEquality().hash(count));

  @JsonKey(ignore: true)
  @override
  _$$_CategoriesDataModelCopyWith<_$_CategoriesDataModel> get copyWith =>
      __$$_CategoriesDataModelCopyWithImpl<_$_CategoriesDataModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CategoriesDataModelToJson(this);
  }
}

abstract class _CategoriesDataModel implements CategoriesDataModel {
  const factory _CategoriesDataModel(
      {required final List<String> categories,
      required final int count}) = _$_CategoriesDataModel;

  factory _CategoriesDataModel.fromJson(Map<String, dynamic> json) =
      _$_CategoriesDataModel.fromJson;

  @override
  List<String> get categories => throw _privateConstructorUsedError;
  @override
  int get count => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_CategoriesDataModelCopyWith<_$_CategoriesDataModel> get copyWith =>
      throw _privateConstructorUsedError;
}
