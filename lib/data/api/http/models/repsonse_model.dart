class ResponseModel {
  ResponseModel({
    required this.data,
    required this.statusCode,
  });

  final Map<String, dynamic>? data;
  final int? statusCode;
}
