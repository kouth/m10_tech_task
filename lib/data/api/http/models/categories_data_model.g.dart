// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categories_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CategoriesDataModel _$$_CategoriesDataModelFromJson(
        Map<String, dynamic> json) =>
    _$_CategoriesDataModel(
      categories: (json['categories'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      count: json['count'] as int,
    );

Map<String, dynamic> _$$_CategoriesDataModelToJson(
        _$_CategoriesDataModel instance) =>
    <String, dynamic>{
      'categories': instance.categories,
      'count': instance.count,
    };
