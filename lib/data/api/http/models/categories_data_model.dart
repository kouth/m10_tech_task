import 'package:freezed_annotation/freezed_annotation.dart';

part 'categories_data_model.freezed.dart';
part 'categories_data_model.g.dart';

@freezed
class CategoriesDataModel with _$CategoriesDataModel {
  const factory CategoriesDataModel({
    required List<String> categories,
    required int count,
  }) = _CategoriesDataModel;

  factory CategoriesDataModel.fromJson(Map<String, Object?> json) =>
      _$CategoriesDataModelFromJson(json);
}
