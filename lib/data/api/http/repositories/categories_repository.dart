import 'package:injectable/injectable.dart';

import '../app_dio.dart';
import '../models/repsonse_model.dart';

@Injectable()
class CategoriesRepository {
  CategoriesRepository(
    this._dio,
  );

  final AppDio _dio;

  Future<ResponseModel> getSummary() async {
    final _response = await _dio.getRequest(
      'categories',
    );
    return ResponseModel(
      data: _response?.data as Map<String, dynamic>?,
      statusCode: _response?.statusCode,
    );
  }
}
