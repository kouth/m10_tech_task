// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Очистить базу`
  String get clearDb {
    return Intl.message(
      'Очистить базу',
      name: 'clearDb',
      desc: '',
      args: [],
    );
  }

  /// `Дата`
  String get date {
    return Intl.message(
      'Дата',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Введите логин`
  String get enterLogin {
    return Intl.message(
      'Введите логин',
      name: 'enterLogin',
      desc: '',
      args: [],
    );
  }

  /// `Введите имя`
  String get enterName {
    return Intl.message(
      'Введите имя',
      name: 'enterName',
      desc: '',
      args: [],
    );
  }

  /// `Введите пароль`
  String get enterPassword {
    return Intl.message(
      'Введите пароль',
      name: 'enterPassword',
      desc: '',
      args: [],
    );
  }

  /// `Ошибка`
  String get error {
    return Intl.message(
      'Ошибка',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `Ошибка: {error}`
  String errorWithMessage(String error) {
    return Intl.message(
      'Ошибка: $error',
      name: 'errorWithMessage',
      desc: '',
      args: [error],
    );
  }

  /// `Выйти`
  String get exit {
    return Intl.message(
      'Выйти',
      name: 'exit',
      desc: '',
      args: [],
    );
  }

  /// `Заполните все поля`
  String get fillAllFields {
    return Intl.message(
      'Заполните все поля',
      name: 'fillAllFields',
      desc: '',
      args: [],
    );
  }

  /// `Дом`
  String get home {
    return Intl.message(
      'Дом',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Количество элементов: {count}`
  String itemCount(int count) {
    return Intl.message(
      'Количество элементов: $count',
      name: 'itemCount',
      desc: '',
      args: [count],
    );
  }

  /// `Русский`
  String get language {
    return Intl.message(
      'Русский',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Загрузка`
  String get loading {
    return Intl.message(
      'Загрузка',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Загрузка: {loading}`
  String loadingWithMessage(String loading) {
    return Intl.message(
      'Загрузка: $loading',
      name: 'loadingWithMessage',
      desc: '',
      args: [loading],
    );
  }

  /// `Логин`
  String get login {
    return Intl.message(
      'Логин',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Имя`
  String get name {
    return Intl.message(
      'Имя',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Не найден`
  String get notFound {
    return Intl.message(
      'Не найден',
      name: 'notFound',
      desc: '',
      args: [],
    );
  }

  /// `Пароль`
  String get password {
    return Intl.message(
      'Пароль',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Профиль`
  String get profile {
    return Intl.message(
      'Профиль',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Маршрут не найден`
  String get routeNotFound {
    return Intl.message(
      'Маршрут не найден',
      name: 'routeNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Войти`
  String get signIn {
    return Intl.message(
      'Войти',
      name: 'signIn',
      desc: '',
      args: [],
    );
  }

  /// `Введите данные чтобы авторизоваться`
  String get signInDescription {
    return Intl.message(
      'Введите данные чтобы авторизоваться',
      name: 'signInDescription',
      desc: '',
      args: [],
    );
  }

  /// `Зарегистрироваться`
  String get signUp {
    return Intl.message(
      'Зарегистрироваться',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// `Введите данные чтобы зарегистрироваться`
  String get signUpDescription {
    return Intl.message(
      'Введите данные чтобы зарегистрироваться',
      name: 'signUpDescription',
      desc: '',
      args: [],
    );
  }

  /// `Успех`
  String get success {
    return Intl.message(
      'Успех',
      name: 'success',
      desc: '',
      args: [],
    );
  }

  /// `Успех: {success}`
  String successWithMessage(String success) {
    return Intl.message(
      'Успех: $success',
      name: 'successWithMessage',
      desc: '',
      args: [success],
    );
  }

  /// `Пользователь существует`
  String get userExists {
    return Intl.message(
      'Пользователь существует',
      name: 'userExists',
      desc: '',
      args: [],
    );
  }

  /// `Профиль пользователя: {login}`
  String userProfile(String login) {
    return Intl.message(
      'Профиль пользователя: $login',
      name: 'userProfile',
      desc: '',
      args: [login],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
