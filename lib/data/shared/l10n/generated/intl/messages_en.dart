// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(error) => "Error: ${error}";

  static String m1(count) => "Item count: ${count}";

  static String m2(loading) => "Loading: ${loading}";

  static String m3(success) => "Success: ${success}";

  static String m4(login) => "User profile: ${login}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "clearDb": MessageLookupByLibrary.simpleMessage("Clear database"),
        "date": MessageLookupByLibrary.simpleMessage("Date"),
        "enterLogin": MessageLookupByLibrary.simpleMessage("Enter your login"),
        "enterName": MessageLookupByLibrary.simpleMessage("Enter a name"),
        "enterPassword": MessageLookupByLibrary.simpleMessage("Enter password"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "errorWithMessage": m0,
        "exit": MessageLookupByLibrary.simpleMessage("Exit"),
        "fillAllFields":
            MessageLookupByLibrary.simpleMessage("Fill in all fields"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "itemCount": m1,
        "language": MessageLookupByLibrary.simpleMessage("English"),
        "loading": MessageLookupByLibrary.simpleMessage("Loading"),
        "loadingWithMessage": m2,
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "notFound": MessageLookupByLibrary.simpleMessage("Not found"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "routeNotFound":
            MessageLookupByLibrary.simpleMessage("Route not found"),
        "signIn": MessageLookupByLibrary.simpleMessage("Sign In"),
        "signInDescription":
            MessageLookupByLibrary.simpleMessage("Enter details to sign in"),
        "signUp": MessageLookupByLibrary.simpleMessage("Sign Up"),
        "signUpDescription":
            MessageLookupByLibrary.simpleMessage("Enter details to sign up"),
        "success": MessageLookupByLibrary.simpleMessage("Success"),
        "successWithMessage": m3,
        "userExists": MessageLookupByLibrary.simpleMessage("User exists"),
        "userProfile": m4
      };
}
