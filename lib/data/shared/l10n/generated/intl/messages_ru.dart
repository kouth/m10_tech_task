// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  static String m0(error) => "Ошибка: ${error}";

  static String m1(count) => "Количество элементов: ${count}";

  static String m2(loading) => "Загрузка: ${loading}";

  static String m3(success) => "Успех: ${success}";

  static String m4(login) => "Профиль пользователя: ${login}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "clearDb": MessageLookupByLibrary.simpleMessage("Очистить базу"),
        "date": MessageLookupByLibrary.simpleMessage("Дата"),
        "enterLogin": MessageLookupByLibrary.simpleMessage("Введите логин"),
        "enterName": MessageLookupByLibrary.simpleMessage("Введите имя"),
        "enterPassword": MessageLookupByLibrary.simpleMessage("Введите пароль"),
        "error": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "errorWithMessage": m0,
        "exit": MessageLookupByLibrary.simpleMessage("Выйти"),
        "fillAllFields":
            MessageLookupByLibrary.simpleMessage("Заполните все поля"),
        "home": MessageLookupByLibrary.simpleMessage("Дом"),
        "itemCount": m1,
        "language": MessageLookupByLibrary.simpleMessage("Русский"),
        "loading": MessageLookupByLibrary.simpleMessage("Загрузка"),
        "loadingWithMessage": m2,
        "login": MessageLookupByLibrary.simpleMessage("Логин"),
        "name": MessageLookupByLibrary.simpleMessage("Имя"),
        "notFound": MessageLookupByLibrary.simpleMessage("Не найден"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "profile": MessageLookupByLibrary.simpleMessage("Профиль"),
        "routeNotFound":
            MessageLookupByLibrary.simpleMessage("Маршрут не найден"),
        "signIn": MessageLookupByLibrary.simpleMessage("Войти"),
        "signInDescription": MessageLookupByLibrary.simpleMessage(
            "Введите данные чтобы авторизоваться"),
        "signUp": MessageLookupByLibrary.simpleMessage("Зарегистрироваться"),
        "signUpDescription": MessageLookupByLibrary.simpleMessage(
            "Введите данные чтобы зарегистрироваться"),
        "success": MessageLookupByLibrary.simpleMessage("Успех"),
        "successWithMessage": m3,
        "userExists":
            MessageLookupByLibrary.simpleMessage("Пользователь существует"),
        "userProfile": m4
      };
}
