import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../../../../app/utils/config/service/l10n/l10n_service.dart';
import 'language_service.dart';

@singleton
class LocaleSettingsController with ChangeNotifier {
  LocaleSettingsController(this._service) {
    loadSettings();
  }

  late Locale _locale;
  final LocaleSettingsService _service;

  Locale get locale => _locale;

  void loadSettings() {
    _locale = _service.locale();

    notifyListeners();
  }

  Future<void> updateLocale() async {
    final localesLength = AppLocale.supportedLocales.length;
    final index = AppLocale.supportedLocales.indexOf(_locale);
    if (index + 1 == localesLength) {
      final newLocale = AppLocale.supportedLocales.elementAt(index - 1);
      if (newLocale == _locale) return;
      _locale = newLocale;
    } else {
      final newLocale = AppLocale.supportedLocales.elementAt(index + 1);
      if (newLocale == _locale) return;
      _locale = newLocale;
    }

    notifyListeners();
  }
}
