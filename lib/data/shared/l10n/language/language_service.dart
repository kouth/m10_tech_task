import 'dart:io';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@singleton
class LocaleSettingsService {
  Locale locale() {
    return Locale.fromSubtags(
      languageCode: Platform.localeName.substring(0, 2),
    );
  }
}
