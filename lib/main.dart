import 'package:flutter/material.dart';

import 'app/application.dart';
import 'app/utils/config/injection/injection.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await configureDependencies();

  runApp(const Application());
}
