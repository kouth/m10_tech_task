# m10_tech_task

A Flutter project for tech task by m10.

## Getting Started

This project is a starting point for a Flutter application.
Projects works on Android & iOS

## Techs and tools

- Storage : shared_prefs
- State : flutter_bloc & provider
- Navigation: vanilla
- Localization: intl
- Networking: dio
- Logger: prettyDioLogger & logger
- Analysis: very_good_analysis
- Codegen: freezed & json_serializable
- DI & SL: injectable & get_it

## API

> API works absolutely free, even without access key etc.

Base url:

```url
https://api.publicapis.org/
```

Endpoint:

```url
https://api.publicapis.org/categories
```

## Need to know

Password stores in sha256 format

## Demo

Sign in screen: 1 screen
Sign up screen: 2 screen
Home screen: 3 screen
Profile screen(ru): 4 Screen
Profile screen(en): 5 Screen

![sign in screen](git_data/1.png)
![sign up screen](git_data/2.png)
![home screen](git_data/3.png)
![profile screen(ru)](git_data/4.png)
![profile screen(en)](git_data/5.png)
